package global

import (
	"gitlab.com/karihirvi/onesec/internal/config"
	"gitlab.com/karihirvi/onesec/internal/util"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

/*
Global package contains shared resources.
*/

// Log provides a logger.
var Log *zap.SugaredLogger

// Conf is a configuration read from multiple yaml files merged together.
var Conf *config.Config

// Init initializes globals. Must be called only once at startup.
func Init(cf *config.Config) {
	Conf = cf
	Log = makeLogger(cf.Production)
}

// makeLogger makes a logger.
func makeLogger(production bool) *zap.SugaredLogger {
	var logger *zap.Logger
	var err error
	if production {
		pc := zap.NewProductionConfig()
		pc.EncoderConfig.EncodeTime = zapcore.RFC3339TimeEncoder
		logger, err = pc.Build()
		// We shouldn't ever get an error back. The call is for not ignoring the error, but
		// still maintaining test coverage.
		util.PanicOnError(err)
	} else {
		logger, _ = zap.NewDevelopment()
	}
	return logger.Sugar()
}
