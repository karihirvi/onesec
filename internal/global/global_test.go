package global

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/karihirvi/onesec/internal/config"
	"testing"
)

func TestMakeLogger(t *testing.T) {
	Init(&config.Config{})
	require.NotNil(t, makeLogger(true))
	require.NotNil(t, makeLogger(false))
}
