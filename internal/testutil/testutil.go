//go:build test

package testutil

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/karihirvi/onesec/internal/config"
	"gitlab.com/karihirvi/onesec/internal/global"
	"gitlab.com/karihirvi/onesec/internal/util"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"
	"path"
	"testing"
)

/*
Package testutil contains code shared between tests.
*/

// Standard locations for test files.

var (
	ExchangeInfoFile string
)

func init() {
	root := path.Join(util.RepoRoot, "test", "testdata")
	ExchangeInfoFile = path.Join(root, "exchange_info.json")
}

// SetupProject reads test config and optionally creates directories with TEST_ROOT environment variable
// as root.
func SetupProject(t *testing.T) {
	configFiles := []string{util.RepoRoot + "/configs/onesec.yaml"}
	conf, err := config.ReadConfig(configFiles)
	require.Nil(t, err)
	conf.Production = false
	global.Init(conf)
}

// SetupObservedLogger sets up logging to memory. Returns observed logs object, which can be used
// to inspect the logs.
func SetupObservedLogger() *observer.ObservedLogs {
	observedZapCore, observedLogs := observer.New(zap.InfoLevel)
	observedLoggerSugared := zap.New(observedZapCore).Sugar()
	global.Log = observedLoggerSugared
	return observedLogs
}
