package binance

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/karihirvi/onesec/internal/global"
	"go.uber.org/atomic"
	"strings"
)

// MarketStreamType defines type for market data streams.
type MarketStreamType int

// BaseEndpoint is the base address for websocket communication.
const BaseEndpoint = "wss://stream.binance.com:9443"

const (
	// MBookTicker is Individual Symbol Book Ticker Streams
	// https://binance-docs.github.io/apidocs/spot/en/#individual-symbol-book-ticker-streams
	MBookTicker MarketStreamType = iota + 1
	// MTrade is Trade Streams
	// https://binance-docs.github.io/apidocs/spot/en/#trade-streams
	MTrade
	// MRollingWindowStat1h is Individual Symbol Rolling Window Statistics Streams with 1h window.
	// https://binance-docs.github.io/apidocs/spot/en/#individual-symbol-rolling-window-statistics-streams
	MRollingWindowStat1h
)

// MarketStreamTypeToString maps MarketStreamType to string.
var MarketStreamTypeToString = map[MarketStreamType]string{
	MBookTicker:          "bookTicker",
	MTrade:               "trade",
	MRollingWindowStat1h: "ticker_1h",
}

// StringToMarketStreamType maps string to MarketStreamType.
var StringToMarketStreamType map[string]MarketStreamType

func init() {
	// Create reverse map.
	StringToMarketStreamType = make(map[string]MarketStreamType)
	for k, v := range MarketStreamTypeToString {
		StringToMarketStreamType[v] = k
	}
}

// NewDefaultStreamManager creates a stream manager using the default BaseEndPoint parameter.
func NewDefaultStreamManager() *StreamManager {
	return NewStreamManager(BaseEndpoint)
}

// NewStreamManager creates a stream manager with the given websocket endpoint.
func NewStreamManager(endPoint string) *StreamManager {
	sm := &StreamManager{
		endPoint: endPoint,
		runState: make(chan runState, 1),
		command:  make(chan streamCommand, 1),
		libCall:  &libCallImpl{},
	}
	sm.runState <- newRunState(nil)
	return sm
}

// StreamSpec defines which stream to subscribe.
type StreamSpec struct {
	Symbol string             // Asset symbol, e.g, "btcusdt"
	Types  []MarketStreamType // What market streams to subscribe for the asset.
}

// StreamManager handles websocket stream communication with Binance servers.
type StreamManager struct {
	endPoint   string             // Base endpoint
	runState   chan runState      // Run state is stored in a channel to serialise access to it.
	cancel     context.CancelFunc // For signaling disconnect gracefully.
	command    chan streamCommand // Subscribe/Unsubscribe command to be sent to Binance.
	nextId     atomic.Int64       // Increasing id for commands.
	libCall    libCall            // Interface to external libraries.
	msgHandled chan struct{}      // To indicate that a message has been handled. Used in testing only.
}

// libCall abstract calls to external libraries which can be mocked for testing purposes.
type libCall interface {
	Marshal(v any) ([]byte, error)                      // json.Marshall signature
	Unmarshal(data []byte, v any) error                 // json.Unmarshall signature
	Decode(input interface{}, output interface{}) error // mapstructure.Decode signature
}

type libCallImpl struct {
}

// Decode is a wrapper for mapstructure.Decode.
func (l libCallImpl) Decode(input interface{}, output interface{}) error {
	return mapstructure.Decode(input, output)
}

// Unmarshal is a wrapper for json.Unmarshal.
func (l libCallImpl) Unmarshal(data []byte, v any) error {
	return json.Unmarshal(data, v)
}

// Marshal is a wrapper for json.Marshal.
func (l libCallImpl) Marshal(v any) ([]byte, error) {
	return json.Marshal(v)
}

// LastError returns an error from the previous streaming sessions, e.g., Connect/Disconnect cycle.
// It's cleared on successful Connect and set when then the streaming stops.
func (s *StreamManager) LastError() (err error) {
	rs := <-s.runState
	err = rs.lastErr
	s.runState <- rs
	return
}

// IsRunning returns true, if websocket loop is running, i.e., sending and receiving messages is possible.
func (s *StreamManager) IsRunning() bool {
	rs := <-s.runState
	running := rs.running
	s.runState <- rs
	return running
}

// newRunState creates a new not-running run state.
func newRunState(lastErr error) runState {
	return runState{
		running: false,
		started: make(chan struct{}),
		stopped: make(chan struct{}),
		lastErr: lastErr,
	}
}

// runState contains a shared state used by StreamManager spawned goroutines.
type runState struct {
	running bool          // True if a websocket loop is running.
	started chan struct{} // Closed channel means the websocket loop has started.
	stopped chan struct{} // Closed channel means the websocket loop has stopped.
	lastErr error         // Error from the last round (e.g., Connect/Disconnect cycle)
}

// Message represents streaming data.
type Message struct {
	Count int64 // Count the number of message since successful connect.
	Data  map[string]interface{}
}

// Connect to websocket streaming service. Returns a buffered message channel of
// length 'bufLen' for receiving data. Client must read the message channel continuously, otherwise data
// will be dropped.
// Dropped data can be detected by inspecting 'Count' field in 'Message', i.e., a difference greater
// than one between consecutive messages indicates a drop. If 'Connect' succeeds, a websocket connection is
// established, but there won't be any data in the message channel yet. The client should
// set up a read loop for the message channel and call 'Subscribe' to receive data.
//
// If the connection attempt fail, error is non-nil.
func (s *StreamManager) Connect(bufLen int) (_ <-chan Message, err error) {
	runStat := <-s.runState
	defer func() {
		if err != nil { // Don't change the state if we made an early return due to an error.
			s.runState <- runStat
		}
	}()
	// Sanity check.
	if runStat.running {
		return nil, fmt.Errorf("already running")
	}
	// Create a TCP connection.
	url := fmt.Sprintf("%v/stream", s.endPoint)
	conn, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		return nil, err
	}
	// After the connection succeeds, start a websocket loop. Note that we're not releasing the runState yet,
	// but passing it to the loop.
	msgCh := make(chan Message, bufLen)
	ctx, cancel := context.WithCancel(context.Background())
	s.cancel = cancel
	go s.websocketLoop(ctx, conn, msgCh, runStat)
	//
	return msgCh, nil
}

// websocketLoop handles communication via the websocket provides as input. The runState is held until a websocket
// reader is running. Received messages are forwarded to msgCh.
func (s *StreamManager) websocketLoop(ctx context.Context, conn *websocket.Conn, msgCh chan Message,
	runStat runState) {
	// Set up a websocket reader goroutine, and after it's running, release runState.
	wsMsgCh := make(chan rawMessage, 1)
	runStat.running = true
	go websocketReader(conn, runStat.started, wsMsgCh) // Reader signals 'started'.
	<-runStat.started
	s.runState <- runStat
	// Bookkeeping variables.
	var msgCount int64
	runningCommands := map[int64]streamCommand{}
	// Websocket handling main loop.
	var err error
forLoop:
	for {
		select {
		case <-ctx.Done(): // Disconnect called.
			_ = conn.Close()
			break forLoop
		case cmd := <-s.command: // Send a command, e.g., subscribe or unsubscribe.
			runningCommands[cmd.Id] = cmd
			if err = sendCommand(s.libCall, cmd, conn); err != nil {
				delete(runningCommands, cmd.Id)
				cmd.resultHandler(err)
			}
		case msg := <-wsMsgCh: // Websocket message
			if err = msg.err; err != nil { // Websocket error.
				break forLoop
			}
			if err = handleMessage(s.libCall, msg.data, &runningCommands, msgCh, &msgCount); err != nil {
				break forLoop
			}
			s.testMsgHandled() // In tests, maybe block on this line.
		}
	}
	// Now we're exiting this function, so reset runState.
	runStat = <-s.runState
	// Signal loop end to whoever is listening.
	close(runStat.stopped)
	close(msgCh)
	// Setup for the next round means creating a new runState. Keep the error from this round.
	s.runState <- newRunState(err)
}

// testMsgHandled optionally blocks execution for testing purposes.
func (s *StreamManager) testMsgHandled() {
	if s.msgHandled != nil {
		s.msgHandled <- struct{}{}
	}
}

// sendCommand send a command to websocket connection.
func sendCommand(lib libCall, cmd streamCommand, conn *websocket.Conn) error {
	cmdJson, err := lib.Marshal(cmd)
	if err != nil {
		return err
	}
	return conn.WriteMessage(websocket.TextMessage, cmdJson)
}

// messageSplitter is a data structure combining command result and stream data.
type messageSplitter struct {
	Result     interface{}            `mapstructure:"result"`
	Id         *int64                 `mapstructure:"id"`
	StreamData map[string]interface{} `mapstructure:"streamData,remain"`
}

// handleMessage inspects if the received message is a command result or stream data, and forwards
// it to its listener.
func handleMessage(lib libCall, rawMsg []byte, runningCommands *map[int64]streamCommand,
	streamResult chan Message, msgCount *int64) error {
	// Bytes to json.
	jsonData := map[string]interface{}{}
	if err := lib.Unmarshal(rawMsg, &jsonData); err != nil {
		return err
	}
	// Split json to command results and stream data.
	var msgSplit messageSplitter
	if err := lib.Decode(jsonData, &msgSplit); err != nil {
		return err
	}
	// Handle command result.
	if len(jsonData) == 2 && msgSplit.Id != nil {
		id := *msgSplit.Id
		cmd, found := (*runningCommands)[id]
		// Possibly a spurious command response from the server.
		if !found {
			global.Log.Warnf("received a command result with id = %v, but not such command was sent", id)
			return nil
		}
		cmd.resultHandler(msgSplit.Result)
		delete(*runningCommands, id) // Drop the result channel from pending list.
		return nil
	}
	// If this line is reached, it's stream data. Send data non-blocking.
	*msgCount++
	msg := Message{
		Count: *msgCount,
		Data:  msgSplit.StreamData,
	}
	select {
	case streamResult <- msg:
	default:
	}
	return nil
}

// rawMessage contains unfiltered websocket input.
type rawMessage struct {
	data []byte // Raw bytes.
	err  error  // Error in reading a message.
}

// websocketReader reads websocket messages. It's running in its own goroutine, since conn.ReadMessage() blocks.
func websocketReader(conn *websocket.Conn, started chan struct{}, wsMsgCh chan rawMessage) {
	close(started)
	for {
		_, msg, err := conn.ReadMessage()
		wsMsgCh <- rawMessage{
			data: msg,
			err:  err,
		}
		// Exits on error, e.g., when the connection drops.
		if err != nil {
			close(wsMsgCh)
			return
		}
	}
}

// WaitForStarted blocks until a websocket stream is started.
func (s *StreamManager) WaitForStarted() {
	rs := <-s.runState
	started := rs.started
	s.runState <- rs
	<-started
}

// WaitForStopped blocks until a websocket stream is stopped.
func (s *StreamManager) WaitForStopped() {
	rs := <-s.runState
	stopped := rs.stopped
	s.runState <- rs
	<-stopped
}

// Disconnect from a websocket streaming service.
func (s *StreamManager) Disconnect() {
	rs := <-s.runState
	defer func() { s.runState <- rs }()
	if s.cancel != nil {
		s.cancel()
	}
}

// streamCommand describe a stream command.
// https://binance-docs.github.io/apidocs/spot/en/#live-subscribing-unsubscribing-to-streams
type streamCommand struct {
	Method        string                   `json:"method"` // "method"
	Params        []string                 `json:"params"` // "params"
	Id            int64                    `json:"id"`     // "id"
	resultHandler func(result interface{}) // Handler for the "result" field.
}

// streamCommand methods.
const (
	mSubscribe   = "SUBSCRIBE"
	mUnsubscribe = "UNSUBSCRIBE"
)

// Subscribe to a list of symbols given in 'specs'. Return an error channel which the client should read.
// Receiving a nil error indicates success.
func (s *StreamManager) Subscribe(specs []StreamSpec) chan error {
	return s.commandImpl(mSubscribe, specs)
}

// Unsubscribe a list of symbols given in 'specs'. Return an error channel which the client should read.
// Receiving a nil error indicates success.
func (s *StreamManager) Unsubscribe(specs []StreamSpec) chan error {
	return s.commandImpl(mUnsubscribe, specs)
}

func (s *StreamManager) commandImpl(method string, specs []StreamSpec) (cmdRet chan error) {
	cmdRet = make(chan error, 1)
	rs := <-s.runState
	running := rs.running
	s.runState <- rs
	if !running {
		cmdRet <- fmt.Errorf("stream manager is not running")
		return
	}
	/* Example message:
	   {
	       "method": "SUBSCRIBE",
	       "params": ["btcusdt@bookTicker", "btcusdt@trade", "btcusdt@ticker_1h"],
	       "id": 1
	   }
	*/
	// Construct strings for the "params".
	var streams []string
	for _, spec := range specs {
		for _, streamType := range spec.Types {
			if _, found := MarketStreamTypeToString[streamType]; !found {
				panic(fmt.Sprintf("unknown stream type '%v'", streamType))
			}
			streams = append(streams, fmt.Sprintf("%s@%s", strings.ToLower(spec.Symbol), MarketStreamTypeToString[streamType]))
		}
	}

	id := s.nextId.Add(1)
	cmd := streamCommand{
		Method: method,
		Params: streams,
		Id:     id,
		resultHandler: func(result interface{}) {
			resultHandler(result, cmdRet)
		},
	}
	s.command <- cmd
	return
}

// resultHandler handles the result JSON from binance server and errors from this file.
func resultHandler(result interface{}, cmdReturn chan<- error) {
	var err error
	if result != nil {
		if errStr, ok := result.(string); ok { // Error from Binance backend.
			err = errors.New(errStr)
		} else if err, ok = result.(error); !ok { // Error from this file.
			panic("unknown error type") // Should not happen.
		}
	}
	// Receiving a nil error indicates success.
	cmdReturn <- err
	close(cmdReturn)
}
