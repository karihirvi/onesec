package binance

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"gitlab.com/karihirvi/onesec/internal/config"
	"io"
	"net/http"
	"time"
)

// GetExchangeInfo returns exchange info
func GetExchangeInfo() ([]byte, error) {
	resp, err := http.Get(UrlBinanceExchangeInfo)
	if err != nil {
		return nil, failedToCallHttp(UrlBinanceExchangeInfo, err)
	}
	body, err2 := io.ReadAll(resp.Body)
	if err2 != nil {
		return nil, failedToReadResponse(err2)
	}
	return body, nil
}

func failedToReadResponse(err error) error {
	return fmt.Errorf("error reading HTTP response: %w", err)
}

// GetAccountInfo returns account info
func GetAccountInfo(cred config.Credentials) ([]byte, error) {
	bn := BinRequest{
		Credentials: cred,
	}
	client := &http.Client{}
	req, err := bn.NewGetRequest(UrlBinanceAccount)
	if err != nil {
		return nil, fmt.Errorf("error creating a new HTTP request, error: %w", err)
	}
	resp, err2 := client.Do(req)
	if err2 != nil {
		return nil, failedToCallHttp(UrlBinanceAccount, err2)
	}
	body, err3 := io.ReadAll(resp.Body)
	if err3 != nil {
		return nil, failedToReadResponse(err3)
	}
	return body, nil
}

// BinRequest struct wraps functionality needed for doing signed requests on Binance API.
// https://binance-docs.github.io/apidocs/spot/en/#signed-trade-user_data-and-margin-endpoint-security
type BinRequest struct {
	Credentials config.Credentials
	Payload     []KeyValue
}

type KeyValue struct {
	Key   string
	Value string
}

func (b *BinRequest) NewGetRequest(url string) (*http.Request, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%v?%v", url,
		string(makePayload(b.Payload, b.Credentials.SecretKey))), nil)
	if err != nil {
		return nil, err
	}
	b.addDefaultHeader(req)
	return req, nil
}

func (b *BinRequest) NewPostRequest(url string) (*http.Request, error) {
	req, err := http.NewRequest("POST", url, bytes.NewReader(makePayload(b.Payload, b.Credentials.SecretKey)))
	if err != nil {
		return nil, err
	}
	b.addDefaultHeader(req)
	return req, nil
}

func (b *BinRequest) addDefaultHeader(req *http.Request) {
	req.Header.Add("X-MBX-APIKEY", b.Credentials.ApiKey)
}

func makePayload(body []KeyValue, secret string) []byte {
	var str string
	for i := 0; i < len(body); i++ {
		str += fmt.Sprintf("%v=%v&", body[i].Key, body[i].Value)
	}
	str += fmt.Sprintf("timestamp=%v", time.Now().UnixMilli())
	str += fmt.Sprintf("&signature=%v", generateSignature(str, secret))
	return []byte(str)
}

func generateSignature(payload, secret string) string {
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(payload))
	return hex.EncodeToString(h.Sum(nil))
}

// getUrl calls URL and parses response body.
func getUrl(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, failedToCallHttp(url, err)
	}
	return parseResponse(resp)
}

func failedToCallHttp(url string, err error) error {
	return fmt.Errorf("error calling URL '%s': %w", url, err)
}

// parseResponse parses a HTTP response.
func parseResponse(resp *http.Response) ([]byte, error) {
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading HTTP response: %w", err)
	}
	return body, nil
}
