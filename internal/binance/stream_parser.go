package binance

import (
	"fmt"
	"gitlab.com/karihirvi/onesec/internal/global"
	"go.uber.org/zap"
	"strings"
	"time"
)

func NewDefaultStreamParser() *StreamParser {
	return NewStreamParser(global.Log)
}

func NewStreamParser(log *zap.SugaredLogger) *StreamParser {
	return &StreamParser{
		log:                 log,
		libCall:             &libCallImpl{},
		BookTickerCh:        make(chan *BookTicker, 1),
		TradeCh:             make(chan *Trade, 1),
		RollingWindowStatCh: make(chan *RollingWindowStat, 1),
		ErrorCh:             make(chan error, 1),
	}
}

type StreamParser struct {
	log                 *zap.SugaredLogger      // Logger
	libCall             libCall                 // Interface to external libraries.
	BookTickerCh        chan *BookTicker        // For parsed BookTicker structs
	TradeCh             chan *Trade             // For parsed Trade structs
	RollingWindowStatCh chan *RollingWindowStat // For parsed RollingWindowStat structs
	ErrorCh             chan error              // For errors
}

// Parse takes a map of data produced by the StreamManager and converts it to an appropriate struct.
func (p *StreamParser) Parse(data map[string]interface{}) (event any, err error) {
	return parseImpl(p.libCall, data)
}

// Send sends a parsed struct to the appropriate channel. The caller must ensure that the struct is of the
// correct type, e.g., output from Parse. The caller must also ensure that the channels are read continuously.
func (p *StreamParser) Send(event any) {
	switch msg := event.(type) {
	case *BookTicker:
		p.BookTickerCh <- msg
	case *Trade:
		p.TradeCh <- msg
	case *RollingWindowStat:
		p.RollingWindowStatCh <- msg
	default:
		panic("unknown struct type in Send") // If this happens, it's a bug.
	}
}

// parseImpl is the implementation of Parse.
// Example of data: {"stream":"btcusdt@bookTicker","data":{"u":37395579592,"s":"BTCUSDT","b":"26408.64000000",
//
//	"B":"2.31114000","a":"26408.65000000","A":"6.47124000"}}
func parseImpl(lib libCall, data map[string]interface{}) (event any, err error) {
	checkField := func(field string) error {
		if _, found := data[field]; !found {
			return fmt.Errorf("no '%v' field in message", field)
		}
		return nil
	}
	if err = checkField("stream"); err != nil {
		return
	}
	if err = checkField("data"); err != nil {
		return
	}
	stream, ok := data["stream"].(string)
	if !ok {
		return nil, fmt.Errorf("'stream' field is not a string")
	}
	parts := strings.Split(stream, "@")
	if len(parts) != 2 {
		return nil, fmt.Errorf("'stream' field is not in the format of 'symbol@type'")
	}
	typ := StringToMarketStreamType[parts[1]]
	event = createStructFromType(typ)
	var jsonStr []byte
	jsonStr, err = lib.Marshal(data["data"])
	if err != nil {
		return
	}
	err = lib.Unmarshal(jsonStr, event)
	if err != nil {
		return
	}
	updateTimestamps(event)
	return
}

func updateTimestamps(msgBase any) {
	now := time.Now()
	switch msg := msgBase.(type) {
	case *BookTicker:
		msg.ReceivedTime = now
		msg.Timestamp = now
	case *Trade:
		msg.ReceivedTime = now
		msg.Timestamp = time.UnixMilli(msg.TradeTime)
	case *RollingWindowStat:
		msg.ReceivedTime = now
		msg.Timestamp = time.UnixMilli(msg.EventTime)
	default:
		panic("unknown struct type in updateTimestamps") // If this happens, it's a bug.
	}
}

// createStructFromType creates a struct of the correct type based on the stream type.
func createStructFromType(typ MarketStreamType) any {
	switch typ {
	case MBookTicker:
		return &BookTicker{}
	case MTrade:
		return &Trade{}
	case MRollingWindowStat1h:
		return &RollingWindowStat{}
	default:
		panic("unknown market stream type") // If this happens, it's a bug.
	}
}
