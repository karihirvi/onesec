package binance

import (
	"github.com/shopspring/decimal"
	"time"
)

type AssetPairSpec struct {
	Symbol              string // Symbol
	BaseAsset           string // Base asset
	BaseAssetPrecision  int
	QuoteAsset          string // Quote asset
	QuoteAssetPrecision int
}

// SymbolStat describes symbol statistics over a 24h moving window.
type SymbolStat struct {
	Symbol        string        // Symbol
	TradeCount    int64         // Total trade count over the 24h window.
	Frequency     float64       // Average trade frequency (trades per second) over the 24h window.
	TradeInterval time.Duration // Average time between trades over the 24h window.
	OpenTime      time.Time     // Start of the 24h window.
	CloseTime     time.Time     // End of the 24h window.
}

// TradeAsset binds together an asset pair and statistics about it.
type TradeAsset struct {
	Spec *AssetPairSpec // Asset pair spec.
	Stat *SymbolStat    // Symbol stats.
}

// TradeLoop is a list of
type TradeLoop struct {
	SymbolStats []*SymbolStat
}

// MessageBase describes fields common to all ticker and other types.
type MessageBase struct {
	// Document `bson:",inline"`
	// When the ticker was received on this server, i.e. NOT when it was created in the exchange's server.
	ReceivedTime time.Time `mapstructure:"received_time" json:"received_time" bson:"received_time"`
	// If the message has a field indicating time at Binance servers, then this is that time.
	// For messages that do not have such a field, this is the same as ReceivedTime.
	// Used as time field in MongoDB collection.
	Timestamp time.Time `bson:"timestamp"`
}

type TypedEvent struct {
	MessageBase `json:",inline" bson:",inline"`
	EventType   string `json:"e" bson:"event_type"` // Event type, metadata in MongoDB collection.
	EventTime   int64  `json:"E" bson:"event_time"` // Event time
	Symbol      string `json:"s" bson:"symbol"`     // Symbol
}

// BookTicker wraps data from individual book ticker streams.
// https://binance-docs.github.io/apidocs/spot/en/#individual-symbol-book-ticker-streams
type BookTicker struct {
	MessageBase     `json:",inline" bson:",inline"`
	UpdateId        int64           `json:"u" bson:"update_id"`         // Order book update id
	Symbol          string          `json:"s" bson:"symbol"`            // Symbol, metadata in MongoDB collection.
	BestBidPrice    decimal.Decimal `json:"b" bson:"best_bid_price"`    // Best bid price
	BestBidQuantity decimal.Decimal `json:"B" bson:"best_bid_quantity"` // Best bid quantity
	BestAskPrice    decimal.Decimal `json:"a" bson:"best_ask_price"`    // Best ask price
	BestAskQuantity decimal.Decimal `json:"A" bson:"best_ask_quantity"` // Best ask quantity
}

// Trade wraps data from trade streams.
// https://binance-docs.github.io/apidocs/spot/en/#trade-streams
type Trade struct {
	TypedEvent         `json:",inline" bson:",inline"`
	TradeId            int64           `json:"t" bson:"trade_id"`
	Price              decimal.Decimal `json:"p" bson:"price"`
	Quantity           decimal.Decimal `json:"q" bson:"quantity"`
	BuyerOrderId       int64           `json:"b" bson:"buyer_order_id"`
	SellerOrderId      int64           `json:"a" bson:"seller_order_id"`
	TradeTime          int64           `json:"T" bson:"trade_time"`
	IsBuyerMarkerMaker bool            `json:"m" bson:"is_buyer_marker_maker"`
}

// RollingWindowStat wraps data from rolling window statistics streams.
// https://binance-docs.github.io/apidocs/spot/en/#individual-symbol-rolling-window-statistics-streams
type RollingWindowStat struct {
	TypedEvent           `json:",inline" bson:",inline"`
	PriceChange          decimal.Decimal `json:"p" bson:"price_change"`           // Price change
	PriceChangePercent   decimal.Decimal `json:"P" bson:"price_change_percent"`   // Price change percent
	OpenPrice            decimal.Decimal `json:"o" bson:"open_price"`             // Open price
	HighPrice            decimal.Decimal `json:"h" bson:"high_price"`             // High price
	LowPrice             decimal.Decimal `json:"l" bson:"low_price"`              // Low price
	LastPrice            decimal.Decimal `json:"c" bson:"last_price"`             // Last price
	WeightedAveragePrice decimal.Decimal `json:"w" bson:"weighted_average_price"` // Weighted average price
	BaseAssetVolume      decimal.Decimal `json:"v" bson:"base_asset_volume"`      // Total traded base asset volume
	QuoteAssetVolume     decimal.Decimal `json:"q" bson:"quote_asset_volume"`     // Total traded quote asset volume
	StatisticsOpenTime   int64           `json:"O" bson:"statistics_open_time"`   // Statistics open time
	StatisticsCloseTime  int64           `json:"C" bson:"statistics_close_time"`  // Statistics close time
	FirstTradeId         int64           `json:"F" bson:"first_trade_id"`         // First trade id
	LastTradeId          int64           `json:"L" bson:"last_trade_id"`          // Last trade id
	NumberOfTrades       int64           `json:"n" bson:"number_of_trades"`       // Total number of trades
}
