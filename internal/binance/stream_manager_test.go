package binance

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/mitchellh/mapstructure"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/require"
	"gitlab.com/karihirvi/onesec/internal/testutil"
	"go.uber.org/atomic"
	"golang.org/x/exp/rand"
	"net/http"
	"sync"
	"testing"
	"time"
)

const port = ":1234"
const mockWebsocketServer = "ws://localhost" + port

func newStreamManagerWithMocks() *StreamManager {
	sm := NewStreamManager(mockWebsocketServer)
	sm.libCall = &libCallMock{}
	return sm
}

func TestStreamManagerCreate(t *testing.T) {
	NewDefaultStreamManager() // for coverage
}

func TestStreamManager_Connect(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	_, err := sm.Connect(1)
	require.NoError(t, err)
	sm.WaitForStarted()
	require.True(t, sm.IsRunning())
	sm.Disconnect()
	sm.WaitForStopped()
	require.NoError(t, sm.LastError())
	require.False(t, sm.IsRunning())
	serv.shutdown()
	time.Sleep(10 * time.Millisecond) // Helps in setting breakpoints.
}

func TestStreamManager_Connect2(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	for i := 0; i < 5; i++ {
		require.False(t, sm.IsRunning())
		_, err := sm.Connect(1)
		require.NoError(t, err)
		sm.WaitForStarted()
		require.True(t, sm.IsRunning())
		sm.Disconnect()
		require.NoError(t, sm.LastError())
		sm.WaitForStopped()
	}
	require.False(t, sm.IsRunning())
	serv.shutdown()
	time.Sleep(10 * time.Millisecond) // Helps in setting breakpoints.
}

func TestStreamManager_Connect3(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	_, err := sm.Connect(1)
	require.NoError(t, err)
	sm.WaitForStarted()
	_, err2 := sm.Connect(1)
	require.ErrorContains(t, err2, "already running")
	sm.Disconnect()
	sm.WaitForStopped()
	require.NoError(t, sm.LastError())
	serv.shutdown()
}

func TestStreamManager_ConnectError(t *testing.T) {
	sm := newStreamManagerWithMocks()
	_, err := sm.Connect(1)
	require.ErrorContains(t, err, "dial tcp [::1]:1234: connect: connection refused")
}

func TestSubscribe(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	sm.libCall = libCallImpl{} // Use real implementation for coverage.
	_, err := sm.Connect(1)
	require.NoError(t, err)
	errCh := sm.Subscribe([]StreamSpec{{Symbol: "btcusdt", Types: []MarketStreamType{MBookTicker}}})
	err2 := <-errCh
	require.NoError(t, err2)
	serv.shutdown()
}

func TestUnsubscribe(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	_, err := sm.Connect(1)
	require.NoError(t, err)
	// serv.cmdResponse = "error executing command"
	errCh := sm.Unsubscribe([]StreamSpec{{Symbol: "btcusdt", Types: []MarketStreamType{MBookTicker}}})
	err2 := <-errCh
	require.NoError(t, err2)
	serv.shutdown()
}

func TestSubscribeErrorResponse(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	defer serv.shutdown()
	sm := newStreamManagerWithMocks()
	_, err := sm.Connect(1)
	require.NoError(t, err)
	serv.cmdResponse = "error executing command"
	errCh := sm.Subscribe([]StreamSpec{{Symbol: "btcusdt", Types: []MarketStreamType{MBookTicker}}})
	err2 := <-errCh
	require.ErrorContains(t, err2, serv.cmdResponse)
}

func TestSubscribeButNoResponseInTime(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	_, err := sm.Connect(1)
	require.NoError(t, err)
	// Shutdown server early
	serv.shutdown()

	errCh := sm.Subscribe([]StreamSpec{{Symbol: "btcusdt", Types: []MarketStreamType{MBookTicker}}})
	timeoutSelected := false
	select {
	case <-errCh:
		require.Fail(t, "timeout should happen")
	case <-time.After(time.Second):
		timeoutSelected = true

	}
	require.True(t, timeoutSelected)
}

func TestMarshalFailed(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	_, err := sm.Connect(1)
	require.NoError(t, err)
	// Set up a serialization failure
	errTxt := "marshal failed"
	sm.libCall.(*libCallMock).marshalFailed = errTxt
	//
	errCh := sm.Subscribe([]StreamSpec{{Symbol: "btcusdt", Types: []MarketStreamType{MBookTicker}}})
	require.EqualError(t, <-errCh, errTxt)
	require.True(t, sm.IsRunning()) // Marshal errors don't exit the websocket loop.
	serv.shutdown()
}

func TestUnmarshalFailed(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	_, err := sm.Connect(1)
	require.NoError(t, err)
	// Set up a deserialization failure and send response to a command that's not sent.
	errTxt := "unmarshal failed"
	sm.libCall.(*libCallMock).unmarshalFailed = errTxt
	serv.idResponse <- 100
	//
	sm.WaitForStopped()
	require.False(t, sm.IsRunning())
	require.EqualError(t, sm.LastError(), errTxt)
	serv.shutdown()
}

func TestDecodeFailed(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	_, err := sm.Connect(1)
	require.NoError(t, err)
	// Set up a deserialization failure and send response to a command that's not sent.
	errTxt := "decode failed"
	sm.libCall.(*libCallMock).decodeFailed = errTxt
	serv.idResponse <- 100
	//
	sm.WaitForStopped()
	require.False(t, sm.IsRunning())
	require.EqualError(t, sm.LastError(), errTxt)
	serv.shutdown()
}

func TestSpuriousCommandFromServerWarning(t *testing.T) {
	observedLogs := testutil.SetupObservedLogger()
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	_, err := sm.Connect(1)
	require.NoError(t, err)
	// Send response to a command that's not sent.
	sm.msgHandled = make(chan struct{})
	serv.idResponse <- 100
	//
	<-sm.msgHandled
	require.Equal(t, 1, observedLogs.Len())
	require.Contains(t, observedLogs.All()[0].Message, "received a command result with id =")
	serv.shutdown()
}

func TestSubscribeErrorNotRunning(t *testing.T) {
	sm := newStreamManagerWithMocks()
	errCh := sm.Subscribe([]StreamSpec{{Symbol: "btcusdt", Types: []MarketStreamType{MBookTicker}}})
	err2 := <-errCh
	require.ErrorContains(t, err2, "stream manager is not running")
}

func TestStreaming(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	msgCh, err := sm.Connect(1)
	require.NoError(t, err)
	// No need to subscribe for the mock.
	entered, exited := make(chan struct{}), make(chan struct{})
	N := 10
	var messages []Message
	go func() {
		close(entered)
		defer close(exited)
		count := 0
		for {
			if count == N {
				return
			}
			select {
			case msg := <-msgCh:
				count++
				messages = append(messages, msg)
			}
		}
	}()
	<-entered
	serv.sendData.Store(true)
	<-exited
	require.Equal(t, N, len(messages))
	serv.shutdown()
}

func TestStreamingAndDroppedMessagesDueToSlowReading(t *testing.T) {
	serv := newMockServer(t)
	serv.serve()
	sm := newStreamManagerWithMocks()
	sm.msgHandled = make(chan struct{})
	bufLen := 5
	msgCh, err := sm.Connect(bufLen)
	require.NoError(t, err)
	// Fill the receive buffer
	fillF := func() {
		for i := 0; i < bufLen; i++ {
			serv.sendOneTicker <- struct{}{}
			<-sm.msgHandled // wait until message is handled
		}
	}
	fillF()
	// Buffer should be full, test that no messages were dropped.
	drainAndVerifyNoDropped := func() (lastCount int64) {
		prevMsg := <-msgCh
		for i := 0; i < bufLen-1; i++ {
			nextMsg := <-msgCh
			require.Equal(t, int64(1), nextMsg.Count-prevMsg.Count)
			lastCount = nextMsg.Count
			prevMsg = nextMsg
		}
		return
	}
	_ = drainAndVerifyNoDropped()
	// Fill again and then some
	fillF()
	serv.sendOneTicker <- struct{}{}
	<-sm.msgHandled
	serv.sendOneTicker <- struct{}{}
	<-sm.msgHandled
	// Now we should have bufLen message not dropped, but after that we should find 2 dropped messages
	lastCount := drainAndVerifyNoDropped()
	serv.sendOneTicker <- struct{}{}
	<-sm.msgHandled
	msg := <-msgCh
	require.Equal(t, int64(3), msg.Count-lastCount)
	serv.shutdown()
}

func TestResultHandlerPanics(t *testing.T) {
	require.PanicsWithValue(t, "unknown error type", func() {
		resultHandler(1, nil)
	})
}

func TestCommandImplPanics(t *testing.T) {
	sm := newStreamManagerWithMocks()
	rs := <-sm.runState
	rs.running = true // Cheat a bit to make commmandImpl to think that the websocket it running.
	sm.runState <- rs
	streamType := MarketStreamType(10000)
	require.PanicsWithValue(t, fmt.Sprintf("unknown stream type '%v'", streamType), func() {
		sm.commandImpl("", []StreamSpec{{
			Symbol: "",
			Types:  []MarketStreamType{streamType},
		}})
	})
}

// **********************************************************************'

type libCallMock struct {
	decodeFailed    string
	unmarshalFailed string
	marshalFailed   string
}

func (l libCallMock) Decode(input interface{}, output interface{}) error {
	if l.decodeFailed != "" {
		return errors.New(l.decodeFailed)
	}
	return mapstructure.Decode(input, output)
}

func (l libCallMock) Unmarshal(data []byte, v any) error {
	if l.unmarshalFailed != "" {
		return errors.New(l.unmarshalFailed)
	}
	return json.Unmarshal(data, v)
}

func (l libCallMock) Marshal(v any) ([]byte, error) {
	if l.marshalFailed != "" {
		return nil, errors.New(l.marshalFailed)
	}
	return json.Marshal(v)
}

func newMockServer(t *testing.T) *mockServer {
	return &mockServer{
		t:                t,
		shutdownComplete: make(chan struct{}),
		idResponse:       make(chan int64),
		server:           &http.Server{Addr: port},
		sendData:         atomic.NewBool(false),
		dataInterval:     100 * time.Millisecond,
		sendOneTicker:    make(chan struct{}, 1),
	}
}

type mockServer struct {
	t                *testing.T
	server           *http.Server
	cancel           context.CancelFunc
	shutdownComplete chan struct{}
	cmdResponse      string        // Response send for command, empty string means null
	idResponse       chan int64    // Channel for sending spurious command response,
	sendData         *atomic.Bool  // If true, send generated ticker data over websocket.
	dataInterval     time.Duration // Interval of ticker data
	sendOneTicker    chan struct{} // Signal to send one ticker.
}

func (m *mockServer) serve() {
	ctx, cancel := context.WithCancel(context.Background())
	m.cancel = cancel
	running := make(chan struct{})
	go m.websocketLoop(ctx, running)
	<-running
}

func (m *mockServer) shutdown() {
	_ = m.server.Shutdown(context.Background())
	m.cancel()
	<-m.shutdownComplete
}

type result struct {
	Result interface{} `mapstructure:"result"`
	Id     *int64      `mapstructure:"id"`
}

func (m *mockServer) websocketLoop(ctx context.Context, running chan struct{}) {
	var upgrader = websocket.Upgrader{}
	wg := sync.WaitGroup{}
	mux := http.NewServeMux()
	mux.HandleFunc("/stream", func(w http.ResponseWriter, r *http.Request) {
		wg.Add(1)
		defer wg.Done() // Done when exiting this handler.
		ticker := time.NewTicker(m.dataInterval)
		conn, err := upgrader.Upgrade(w, r, nil)
		require.NoError(m.t, err)

		// Set up a websocket reader goroutine, and after it's running, release runState.
		wsMsgCh := make(chan rawMessage, 1)
		started := make(chan struct{})
		go websocketReader(conn, started, wsMsgCh) // Reader signals 'started'.
		<-started

		for {
			select {

			// Exit
			case <-ctx.Done():
				_ = conn.Close()
				return

			// Read message from the client.
			case msg := <-wsMsgCh:
				if msg.err != nil {
					return
				}
				writeResponse(m.t, conn, msg.data, m.cmdResponse, 0)

			// Send spurious response.
			case id := <-m.idResponse:
				writeResponse(m.t, conn, []byte("{}"), "", id)

			// Send ticker data continuously.
			case <-ticker.C:
				if m.sendData.Load() {
					writeTicker(m.t, conn)
				}

			// Send one ticker.
			case <-m.sendOneTicker:
				writeTicker(m.t, conn)
			}
		}
	})
	close(running)
	m.server.Handler = mux
	require.ErrorIs(m.t, http.ErrServerClosed, m.server.ListenAndServe()) // Blocks on this line.
	wg.Wait()                                                             // Wait for handler done.
	close(m.shutdownComplete)
}

// writeResponse writes given response to websocket. Empty resultStr means null, i.e., all ok.
func writeResponse(t *testing.T, conn *websocket.Conn, msg []byte, resultStr string, id int64) {
	jsonData := map[string]interface{}{}
	require.NoError(t, json.Unmarshal(msg, &jsonData))
	var msgSplit messageSplitter
	require.NoError(t, mapstructure.Decode(jsonData, &msgSplit))

	res := result{
		Id: msgSplit.Id,
	}
	if res.Id == nil {
		res.Id = new(int64)
		*res.Id = 0
	}
	// Maybe override id and result for testing purposes.
	if id > 0 {
		*res.Id = id
	}
	if resultStr != "" {
		res.Result = resultStr
	}

	retJson, err2 := json.Marshal(res)
	require.NoError(t, err2)
	require.NoError(t, conn.WriteMessage(websocket.TextMessage, retJson))
}

var updateId int64

func writeTicker(t *testing.T, conn *websocket.Conn) {
	updateId++
	bt := BookTicker{
		UpdateId:        updateId,
		Symbol:          "btcusdt",
		BestBidPrice:    decimal.NewFromFloat(10 * rand.Float64()),
		BestBidQuantity: decimal.NewFromFloat(1000 * rand.Float64()),
		BestAskPrice:    decimal.NewFromFloat(11 * rand.Float64()),
		BestAskQuantity: decimal.NewFromFloat(1100 * rand.Float64()),
	}
	btJson, err := json.Marshal(bt)
	require.NoError(t, err)
	require.NoError(t, conn.WriteMessage(websocket.TextMessage, btJson))
}
