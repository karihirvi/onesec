package binance

const UrlBinanceV3 = "https://api.binance.com/api/v3"
const UrlBinanceAccount = UrlBinanceV3 + "/account"
const UrlBinanceOrderBook = UrlBinanceV3 + "/depth"
const UrlBinanceExchangeInfo = UrlBinanceV3 + "/exchangeInfo"
const UrlBinanceWsBase = "wss://stream.binance.com:9443"
const UrlBinancePriceStats = UrlBinanceV3 + "/ticker/24hr"
