//go:build integration_test

package binance

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestExchangeInfo_GetFromBinance(t *testing.T) {
	exInfo := NewExchangeInfo()
	require.NoError(t, exInfo.GetFromBinance())
	// NOTE: to update test data, uncomment the line below and run this test.
	// updateExchangeInfoTestData(t, exInfo.info)
}

func TestExchangeInfo_GetFromBinance2(t *testing.T) {
	exInfo := NewExchangeInfo()
	exInfo.url = "badUrl" + exInfo.url
	require.ErrorContains(t, exInfo.GetFromBinance(),
		"error calling URL 'badUrlhttps://api.binance.com/api/v3/exchangeInfo")
}
