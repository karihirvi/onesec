package binance

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestStreamManager_Subscribe(t *testing.T) {
	sm := NewDefaultStreamManager()
	bufLen := 10
	msgCh, err := sm.Connect(bufLen)
	require.NoError(t, err)
	sm.WaitForStarted()
	go func() {
		for msg := range msgCh {
			fmt.Printf("%v\n", msg)
		}
	}()
	resCh := sm.Subscribe([]StreamSpec{
		{
			Symbol: "btcusdt",
			Types:  []MarketStreamType{MBookTicker, MTrade, MRollingWindowStat1h},
		},
		{
			Symbol: "ethusdt",
			Types:  []MarketStreamType{MBookTicker},
		},
	})
	require.Nil(t, <-resCh)
	time.Sleep(time.Second)
	resCh = sm.Unsubscribe([]StreamSpec{
		{
			Symbol: "btcusdt",
			Types:  []MarketStreamType{MBookTicker, MTrade, MRollingWindowStat1h},
		},
	})
	require.Nil(t, <-resCh)
	time.Sleep(time.Second)
	_ = sm.Unsubscribe([]StreamSpec{
		{
			Symbol: "ethusdt",
			Types:  []MarketStreamType{MBookTicker},
		},
	})
	time.Sleep(time.Second)
	sm.Disconnect()
	sm.WaitForStopped()
}
