package binance

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/karihirvi/onesec/internal/testutil"
	"testing"
)

func TestExchangeInfo_ParseAssetPairs(t *testing.T) {
	exInfo := NewExchangeInfo()
	require.NoError(t, exInfo.LoadFromFile(testutil.ExchangeInfoFile))
	pairSpecs, err := exInfo.ParseAssetPairs()
	require.NoError(t, err)
	require.Equal(t, 2229, len(pairSpecs))
}
