package binance

import (
	"encoding/json"
	"fmt"
	"gitlab.com/karihirvi/onesec/internal/storage"
)

// NewExchangeInfo creates a new empty exchange info.
func NewExchangeInfo() *ExchangeInfo {
	return NewExchangeInfoFromBytes(nil)
}

// NewExchangeInfoFromBytes creates a new exchange info with the given data.
func NewExchangeInfoFromBytes(info []byte) *ExchangeInfo {
	return &ExchangeInfo{
		url:         UrlBinanceExchangeInfo,
		JsonStorage: storage.JsonStorage{JsonData: info},
	}
}

// ExchangeInfo provides mean to get exchange info from various sources.
type ExchangeInfo struct {
	storage.JsonStorage        // Info is stored here
	url                 string // URL where to GET the info from.
}

// GetFromBinance gets exchange info from Binance.
func (e *ExchangeInfo) GetFromBinance() (err error) {
	e.JsonData, err = getUrl(e.url)
	return
}

func (e *ExchangeInfo) ParseAssetPairs() (pairSpecs map[string]AssetPairSpec, err error) {
	var infoJson map[string]interface{}
	if err = json.Unmarshal(e.JsonData, &infoJson); err != nil {
		return nil, fmt.Errorf("error unmarshaling exchange info into JSON: %w", err)
	}
	symbolJsonObjs := infoJson["symbols"].([]interface{})
	pairSpecs = make(map[string]AssetPairSpec)
	for _, symbolJsonObj := range symbolJsonObjs {
		symbolObj := symbolJsonObj.(map[string]interface{})
		symbol := symbolObj["symbol"].(string)
		pairSpecs[symbol] = AssetPairSpec{
			Symbol:              symbol,
			BaseAsset:           symbolObj["baseAsset"].(string),
			BaseAssetPrecision:  int(symbolObj["baseAssetPrecision"].(float64)),
			QuoteAsset:          symbolObj["baseAsset"].(string),
			QuoteAssetPrecision: int(symbolObj["quoteAssetPrecision"].(float64)),
		}
	}
	return
}
