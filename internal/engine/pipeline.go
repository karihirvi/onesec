package engine

import "os"

type Pipeline struct {
}

// FileStorageHandler accepts byte slices and writes them to a file.
type FileStorageHandler struct {
	// Path to the file.
	Path string
	// File handle.
	file *os.File
}

// NewFileStorageHandler creates a new FileStorageHandler.
func NewFileStorageHandler(path string) *FileStorageHandler {
	return &FileStorageHandler{
		Path: path,
	}
}

// Create creates the file.
func (f *FileStorageHandler) Create() (err error) {
	f.file, err = os.Create(f.Path)
	return
}

// Handle accepts byte slices and writes them to a file.
func (f *FileStorageHandler) Handle(data []byte) (err error) {
	_, err = f.file.Write(data)
	return
}

// Close closes the file.
func (f *FileStorageHandler) Close() (err error) {
	return f.file.Close()
}

type DatabaseStorageHandler struct {
}
