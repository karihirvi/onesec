package engine

import (
	"gitlab.com/karihirvi/onesec/internal/binance"
	"gitlab.com/karihirvi/onesec/internal/database"
	"go.uber.org/zap"
)

func NewEngine(conf *Config, log *zap.SugaredLogger, dbService database.IDatabase,
	streamManager *binance.StreamManager, streamParser *binance.StreamParser) *Engine {
	eng := &Engine{
		state:         make(chan engineState, 1),
		conf:          conf,
		log:           log,
		dbService:     dbService,
		streamManager: streamManager,
		streamParser:  streamParser,
	}
	eng.state <- engineState{}
	return eng
}

type Engine struct {
	state         chan engineState       // State if this object. Acts also as a synchronisation mechanism.
	conf          *Config                // Config
	log           *zap.SugaredLogger     // Logger
	dbService     database.IDatabase     // Database
	streamManager *binance.StreamManager // Stream manager
	streamParser  *binance.StreamParser  // Stream parser
}

type engineState struct {
	msgChan <-chan binance.Message // Messages from Binance websocket.
	specs   []binance.StreamSpec   // Current stream specs.
}

// Config contains Engine related configuration.
type Config struct {
	StoragePath         string // Path to storage directory.
	StreamingBufferSize int    // Size of streaming buffer.
}

func (e *Engine) Connect() (err error) {
	state := <-e.state
	running := make(chan struct{}, 1)
	state.msgChan, err = e.streamManager.Connect(e.conf.StreamingBufferSize)
	go streamLoop(e.streamParser.BookTickerCh, e.streamParser.TradeCh, e.streamParser.RollingWindowStatCh,
		e.dbService, e.log, running)
	<-running
	go func() {
		running <- struct{}{}
		for msg := range state.msgChan {
			event, err2 := e.streamParser.Parse(msg.Data)
			if err2 == nil {
				e.streamParser.Send(event)
			} else {
				e.log.Errorf("Failed to parse message: %v", err2)
			}
		}
	}()
	<-running
	e.state <- state
	return
}

func (e *Engine) Disconnect() {
	state := <-e.state
	e.streamManager.Disconnect()
	e.state <- state
}

func (e *Engine) Subscribe(specs []binance.StreamSpec, saveToDatabase, saveToDisk bool) (streamId string, err error) {
	state := <-e.state
	err = <-e.streamManager.Subscribe(specs)
	if err == nil {
		state.specs = specs
	}
	e.state <- state
	return
}

func (e *Engine) Unsubscribe() (err error) {
	state := <-e.state
	err = <-e.streamManager.Unsubscribe(state.specs)
	e.state <- state
	return
}

// streamLoop handles incoming messages from Binance websocket.
func streamLoop(bookTickerCh chan *binance.BookTicker, tradeCh chan *binance.Trade,
	rollingWindowStatCh chan *binance.RollingWindowStat,
	db database.IDatabase, log *zap.SugaredLogger, running chan<- struct{}) {
	running <- struct{}{}
	for {
		select {
		case msg, ok := <-bookTickerCh:
			if !ok {
				log.Debug("channel closed")
				return
			}
			if err := db.SaveBookTicker(msg); err != nil {
				log.Errorw("failed to save book ticker", "msg", msg, "err", err)
			}
		case msg, ok := <-tradeCh:
			if !ok {
				log.Debug("channel closed")
				return
			}
			if err := db.SaveTrade(msg); err != nil {
				log.Errorw("failed to save trade", "msg", msg, "err", err)
			}
		case msg, ok := <-rollingWindowStatCh:
			if !ok {
				log.Debug("channel closed")
				return
			}
			if err := db.SaveRollingWindowStat(msg); err != nil {
				log.Errorw("failed to save rolling window stat", "msg", msg, "err", err)
			}
		}
	}
}
