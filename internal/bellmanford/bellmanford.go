package bellmanford

import (
	"fmt"
	"github.com/shopspring/decimal"
	"math"
	"time"
)

/*
Package bellmanford implements Bellmann-Ford arbitrage algorithm.
*/

// Adapted from https://gist.github.com/ewancook/d39a06b2ee6e3f7c7c4d6cb66f2dcff7

type Direction int

const (
	Buy  Direction = iota // You're buying an asset
	Sell                  // You're selling an asset
)

type SymbolUpdate struct {
	Count    int64
	Symbol   string
	Bid, Ask decimal.Decimal
	// Bid    int64 // Bid price as integer, you need to know the multiplier in order to convert to real price
	// Ask    int64 // Quote price as integer
	// Bid    float64 // Bid price
	// Ask    float64 // Quote price
}

type EdgePair struct {
	BuyEdge  *Edge
	SellEdge *Edge
}

// Graph represents a graph consisting of Edges and Vertices
type Graph struct {
	conf             *Config
	Edges            []*Edge
	Vertices         []int
	Assets           []string
	symbolToEdge     map[string]EdgePair
	allWeightsFilled bool
	//
	stats       Stats
	runtimes    []time.Duration
	runtimesInd int
}

type Stats struct {
	SymbolCount    map[string]int64
	RunCount       int64
	AverageRuntime time.Duration
}

type ArbitrageLoop struct {
	Vertices []int
}

// Edge represents a weighted line between two nodes
type Edge struct {
	From, To int
	Weight   float64
	Bid, Ask decimal.Decimal
	Symbol   string
	Direction
}

// NewGraph returns a graph consisting of given Edges and Vertices (Vertices must count from 0 upwards)
func NewGraph(conf *Config, edges []*Edge, vertices []int, assets []string, symbolToEdge map[string]EdgePair) *Graph {
	return &Graph{
		conf:         conf,
		Edges:        edges,
		Vertices:     vertices,
		Assets:       assets,
		symbolToEdge: symbolToEdge,
		stats:        Stats{SymbolCount: map[string]int64{}},
		runtimes:     make([]time.Duration, conf.StatsRuntimeAverage),
	}
}

type Config struct {
	StatsRuntimeAverage int
}

func (g *Graph) GetStatistics() Stats {
	endInd := len(g.runtimes)
	if g.stats.RunCount < int64(g.conf.StatsRuntimeAverage) {
		endInd = int(g.stats.RunCount)
	}
	start := time.Now()
	end := start
	for i := 0; i < endInd; i++ {
		end = end.Add(g.runtimes[i])
	}
	g.stats.AverageRuntime = end.Sub(start)
	return g.stats
}

func (g *Graph) CalculateTradeSequence(vertices []int) (decimal.Decimal, string) {
	findEdge := func(fromId, toId int) (edge *Edge) {
		for _, e := range g.Edges {
			if e.From == fromId && e.To == toId {
				edge = e
				break
			}
		}
		return
	}
	var str string
	factor := decimal.New(1, 0)
	for i := 0; i < len(vertices)-1; i++ {
		edge := findEdge(vertices[i], vertices[i+1])
		if edge.Direction == Buy {
			str += fmt.Sprintf("Buy %v (%v)", edge.Symbol, edge.Ask)
			factor = edge.Ask.Mul(factor)
		} else {
			str += fmt.Sprintf("Sell %v (%v)", edge.Symbol, edge.Bid)
			factor = factor.Div(edge.Bid)
		}
		if i < len(vertices)-2 {
			str += " -> "
		}
	}
	return factor, str
}

func (g *Graph) UpdateEdge(update SymbolUpdate) bool {
	g.stats.SymbolCount[update.Symbol]++
	edge := g.symbolToEdge[update.Symbol]
	askFloat, _ := update.Ask.Float64()
	bidFloat, _ := update.Bid.Float64()
	edge.BuyEdge.Weight = math.Log(askFloat)
	edge.SellEdge.Weight = -math.Log(bidFloat)
	edge.BuyEdge.Ask = update.Ask
	edge.SellEdge.Bid = update.Bid
	if !g.allWeightsFilled {
		symbolsFilled := 0
		for symbol := range g.symbolToEdge {
			if _, found := g.stats.SymbolCount[symbol]; found {
				symbolsFilled++
				continue
			}
			break
		}
		g.allWeightsFilled = symbolsFilled == len(g.symbolToEdge)
	}
	return g.allWeightsFilled
}

// FindArbitrageLoops returns either an arbitrage loop or a nil map
func (g *Graph) FindArbitrageLoops() (loops []ArbitrageLoop) {
	start := time.Now()
	// source := 0 // source can be any vertex, since in our case all vertices are reachable
	for _, source := range g.Vertices {
		predecessors, distances := g.BellmanFord(source)
		if vertices := g.FindNegativeWeightCycle(predecessors, distances, source); vertices != nil {
			loops = append(loops, ArbitrageLoop{Vertices: vertices})
		}
	}

	end := time.Now()
	elapsed := end.Sub(start)
	// elapsed := time.Since(start)
	g.stats.RunCount++
	ind := g.runtimesInd % g.conf.StatsRuntimeAverage
	g.runtimes[ind] = elapsed
	g.runtimesInd++
	return
}

// BellmanFord determines the shortest path and returns the predecessors and distances
func (g *Graph) BellmanFord(source int) ([]int, []float64) {
	size := len(g.Vertices)
	distances := make([]float64, size)
	predecessors := make([]int, size)
	// predecessors := make([]*Vertex, size)
	for _, v := range g.Vertices {
		distances[v] = math.MaxFloat64
	}
	distances[source] = 0

	for i, changes := 0, 0; i < size-1; i, changes = i+1, 0 {
		for _, edge := range g.Edges {
			if newDist := distances[edge.From] + edge.Weight; newDist < distances[edge.To] {
				distances[edge.To] = newDist
				predecessors[edge.To] = edge.From
				changes++
			}
		}
		if changes == 0 {
			break
		}
	}
	return predecessors, distances
}

// FindNegativeWeightCycle finds a negative weight cycle from predecessors and a source
func (g *Graph) FindNegativeWeightCycle(predecessors []int, distances []float64, source int) []int {
	for _, edge := range g.Edges {
		if distances[edge.From]+edge.Weight < distances[edge.To] {
			return arbitrageLoop(predecessors, source)
		}
	}
	return nil
}

func arbitrageLoop(predecessors []int, source int) []int {
	size := len(predecessors)
	loop := make([]int, size)
	loop[0] = source

	exists := make([]bool, size)
	exists[source] = true

	indices := make([]int, size)

	var index, next int
	for index, next = 1, source; ; index++ {
		next = predecessors[next]
		loop[index] = next
		if exists[next] {
			return loop[indices[next] : index+1]
		}
		indices[next] = index
		exists[next] = true
	}
}
