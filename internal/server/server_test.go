package server

import (
	"context"
	"errors"
	"fmt"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/karihirvi/onesec/internal/binance"
	"gitlab.com/karihirvi/onesec/internal/global"
	"gitlab.com/karihirvi/onesec/internal/testutil"
	"gitlab.com/karihirvi/onesec/pb/onesecpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"testing"
	"time"
)

const testServerAddress = ":5353"

func TestServerStartFailures(t *testing.T) {
	testutil.SetupProject(t)
	conf := &Config{
		ServerAddress: "bad address",
		ExitSleep:     time.Second,
	}
	db := &databaseMock{}
	server := NewServer(conf, global.Log, db, nil)
	// Connection failure
	mockCall := db.On("Connect").Return(errors.New("connect error"))
	require.ErrorContains(t, server.Start(), "error connecting to database, error: connect error")
	mockCall.Unset()
	// Initialization failure.
	mockCall = db.On("Connect").Return(nil).
		On("Initialize").Return(errors.New("initialization error"))
	require.ErrorContains(t, server.Start(), "error initializing database, error: initialization error")
	mockCall.Unset()
	// gRPC failure.
	mockCall = db.On("Connect").Return(nil).On("Initialize").Return(nil)
	require.ErrorContains(t, server.Start(), "error creating gRPC server, error: listen tcp: address bad address: "+
		"missing port in address")
	mockCall.Unset()
}

func makeWorkingServer() *Server {
	conf := &Config{
		ServerAddress: testServerAddress,
		ExitSleep:     50 * time.Millisecond,
	}
	db := &databaseMock{}
	server := NewServer(conf, global.Log, db, nil)
	// Services are ok.
	db.On("Connect").Return(nil).On("Initialize").Return(nil).On("Disconnect").Return(nil)
	return server
}

func TestServerStart(t *testing.T) {
	testutil.SetupProject(t)
	server := makeWorkingServer()
	// Start and stop normally.
	go func() {
		require.NoError(t, server.Start())
	}()
	server.WaitForStarted()
	require.True(t, server.running)
	require.NoError(t, server.Stop(false))
	server.WaitForStopped()
	require.False(t, server.running)
}

func TestServerStartMultiple(t *testing.T) {
	testutil.SetupProject(t)
	server := makeWorkingServer()
	for i := 0; i < 6; i++ {
		go func() {
			require.NoError(t, server.Start())
		}()
		immediate := i%2 == 0 // even numbers are immediate
		server.WaitForStarted()
		require.True(t, server.running)
		require.NoError(t, server.Stop(immediate))
		server.WaitForStopped()
		require.False(t, server.running)
	}
}

func TestServerExitWithGrpc(t *testing.T) {
	testutil.SetupProject(t)
	conf := &Config{
		ServerAddress: testServerAddress,
		ExitSleep:     50 * time.Millisecond,
	}
	db := &databaseMock{}
	server := NewServer(conf, global.Log, db, nil)
	db.On("Connect").Return(nil).On("Initialize").Return(nil).On("Disconnect").
		Return(errors.New("error in disconnecting"))
	go func() {
		require.NoError(t, server.Start())
	}()
	server.WaitForStarted()
	// Make a gRPC call.
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock()}
	address := fmt.Sprintf("localhost%s", testServerAddress)
	conn, err := grpc.DialContext(context.Background(), address, opts...)
	require.NoError(t, err)
	client := onesecpb.NewOnesecClient(conn)
	res, err2 := client.Exit(context.Background(), &onesecpb.ExitRequest{StopImmediately: false})
	server.WaitForStopped()
	require.NoError(t, err2)
	require.NotNil(t, res)
}

// *******************************************'

type databaseMock struct {
	mock.Mock
}

func (d *databaseMock) SaveBookTicker(ticker *binance.BookTicker) error {
	// TODO implement me
	panic("implement me")
}

func (d *databaseMock) SaveTrade(trade *binance.Trade) error {
	// TODO implement me
	panic("implement me")
}

func (d *databaseMock) SaveRollingWindowStat(window *binance.RollingWindowStat) error {
	// TODO implement me
	panic("implement me")
}

func (d *databaseMock) SaveExchangeInfo(infoJson []byte) error {
	// TODO implement me
	panic("implement me")
}

func (d *databaseMock) LoadExchangeInfo() (infoJson []byte, whenSaved time.Time, _ error) {
	// TODO implement me
	panic("implement me")
}

func (d *databaseMock) Connect() error {
	args := d.Called()
	return args.Error(0)
}

func (d *databaseMock) Initialize() error {
	args := d.Called()
	return args.Error(0)
}

func (d *databaseMock) Disconnect() error {
	args := d.Called()
	return args.Error(0)
}
