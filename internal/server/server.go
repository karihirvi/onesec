package server

import (
	"fmt"
	"gitlab.com/karihirvi/onesec/internal/database"
	"gitlab.com/karihirvi/onesec/internal/engine"
	"gitlab.com/karihirvi/onesec/pb/onesecpb"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net"
	"sync"
	"time"
)

/*
Server package implements basic logic for starting and stopping a server with a gRPC API.
*/

// gRPCServerStartDelay is a technical delay, see Start() method.
const gRPCServerStartDelay = 10 * time.Millisecond

// NewServer creates a new Server.
func NewServer(conf *Config, log *zap.SugaredLogger, dbService database.IDatabase,
	engine *engine.Engine) *Server {
	return &Server{
		conf:      conf,
		log:       log,
		dbService: dbService,
		engine:    engine,
		runningC:  sync.Cond{L: &sync.Mutex{}},
	}
}

// Config contains Server related configuration.
type Config struct {
	ServerAddress string        // gRPC Server port to listen, e.g, ":4242".
	ExitSleep     time.Duration // Sleep before issuing a gRPC Stop.
}

// Server implements Server logic.
type Server struct {
	mux        sync.Mutex         // Serialise access to this object.
	conf       *Config            // Config
	log        *zap.SugaredLogger // Logger
	grpcServer *grpc.Server       // gRPC Server
	dbService  database.IDatabase // Database service
	engine     *engine.Engine     // Engine
	running    bool               // If true, server is running.
	runningC   sync.Cond          // Condition for running.
}

// Start starts a server and blocks until gRPC server exits.
// Can be started again only after calling WaitForStopped, or results
// are unpredictable.
func (s *Server) Start() (err error) {
	// Database
	if err = s.dbService.Connect(); err != nil {
		return fmt.Errorf("error connecting to database, error: %w", err)
	}
	if err = s.dbService.Initialize(); err != nil {
		return fmt.Errorf("error initializing database, error: %w", err)
	}
	// All gRPC related.
	var lis net.Listener
	lis, err = s.makeGrpcServer()
	if err != nil {
		return fmt.Errorf("error creating gRPC server, error: %w", err)
	}

	s.log.Infow("Starting Onesec server.", "address", s.conf.ServerAddress)
	// Run in a thread with delay to ensure gRPC server is started before to flag is set.
	go func() {
		time.Sleep(gRPCServerStartDelay)
		s.setRunningStatus(true)
	}()
	err = s.grpcServer.Serve(lis) // Blocks on this line.
	s.setRunningStatus(false)
	s.log.Info("Onesec server stopped.")
	return
}

func (s *Server) setRunningStatus(tf bool) {
	s.runningC.L.Lock()
	defer s.runningC.L.Unlock()
	s.running = tf
	s.runningC.Broadcast()
}

func (s *Server) WaitForStarted() {
	s.runningC.L.Lock()
	defer s.runningC.L.Unlock()
	for !s.running {
		s.runningC.Wait()
	}
}

func (s *Server) WaitForStopped() {
	s.runningC.L.Lock()
	defer s.runningC.L.Unlock()
	for s.running {
		s.runningC.Wait()
	}
}

func (s *Server) makeGrpcServer() (lis net.Listener, err error) {
	// Create a gRPC grpcServer API.
	s.grpcServer = grpc.NewServer()
	reflection.Register(s.grpcServer)
	// Callback for exit.
	exitCallback := func(stopImmediately bool) {
		go func() {
			// Run async and sleep to allow gRPC response to be sent.
			time.Sleep(s.conf.ExitSleep)
			if err2 := s.Stop(stopImmediately); err2 != nil {
				s.log.Warnw("Error in exiting", "error", err2)
			}
		}()
	}
	onesecpb.RegisterOnesecServer(s.grpcServer, newOnesecApi(exitCallback, s.engine))
	// Listen to a tcp port.
	lis, err = net.Listen("tcp", fmt.Sprintf(s.conf.ServerAddress))
	return
}

// Stop blocks until the Server has stopped. If immediately is true, forcefully stops gRPC Server
// without waiting for exiting connections to complete.
func (s *Server) Stop(immediately bool) error {
	s.mux.Lock()
	defer s.mux.Unlock()
	// Stop gRPC Server.
	if s.grpcServer != nil {
		if immediately {
			s.log.Info("Stopping server immediately.")
			s.grpcServer.Stop()
		} else {
			s.log.Info("Stopping server gracefully.")
			s.grpcServer.GracefulStop()
		}
		s.grpcServer = nil
	}
	return s.dbService.Disconnect()
}
