package server

import (
	"context"
	"fmt"
	"gitlab.com/karihirvi/onesec/internal/binance"
	"gitlab.com/karihirvi/onesec/internal/engine"
	"gitlab.com/karihirvi/onesec/pb/onesecpb"
)

func newOnesecApi(exitCallback func(immediately bool), engine *engine.Engine) *onesecApi {
	return &onesecApi{exitCallback: exitCallback, engine: engine}
}

type onesecApi struct {
	onesecpb.UnimplementedOnesecServer
	exitCallback func(stopImmediately bool) // Exit callback
	engine       *engine.Engine             // Engine
}

// wrapStatus wraps a Go error into the custom protobuf status message.
func wrapStatus(err error) (errObj *onesecpb.Status) {
	errObj = &onesecpb.Status{}
	if err != nil {
		*errObj.Error = err.Error()
	}
	return
}

var goMarketStreamType = map[onesecpb.MarketStreamType]binance.MarketStreamType{
	onesecpb.MarketStreamType_MARKETSTREAMTYPE_BOOKTICKER:          binance.MBookTicker,
	onesecpb.MarketStreamType_MARKETSTREAMTYPE_TRADE:               binance.MTrade,
	onesecpb.MarketStreamType_MARKETSTREAMTYPE_ROLLINGWINDOWSTAT1H: binance.MRollingWindowStat1h,
}

func (o *onesecApi) Connect(context.Context, *onesecpb.ConnectRequest) (*onesecpb.ConnectResponse, error) {
	return &onesecpb.ConnectResponse{Status: wrapStatus(o.engine.Connect())}, nil
}

func (o *onesecApi) Disconnect(context.Context, *onesecpb.DisconnectRequest) (*onesecpb.DisconnectResponse, error) {
	return &onesecpb.DisconnectResponse{Status: &onesecpb.Status{}}, nil
}

func (o *onesecApi) Subscribe(_ context.Context, request *onesecpb.SubscribeRequest) (
	*onesecpb.SubscribeResponse, error) {
	var specs []binance.StreamSpec
	var err error

forLoop:
	for _, trace := range request.Traces {
		// Convert types to Go types
		var streamTypes []binance.MarketStreamType
		for _, streamType := range trace.Types {
			if t, found := goMarketStreamType[streamType]; found {
				streamTypes = append(streamTypes, t)
			} else {
				err = fmt.Errorf("unknown market stream type '%v'", streamType)
				break forLoop
			}
		}
		specs = append(specs, binance.StreamSpec{
			Symbol: trace.Symbol,
			Types:  streamTypes,
		})
	}
	var streamId string
	if err == nil {
		streamId, err = o.engine.Subscribe(specs, true, true)
	}

	return &onesecpb.SubscribeResponse{
		Status:   wrapStatus(err),
		StreamId: streamId,
	}, nil
}

func (o *onesecApi) Unsubscribe(ctx context.Context, request *onesecpb.UnsubscribeRequest) (
	*onesecpb.UnsubscribeResponse, error) {
	return &onesecpb.UnsubscribeResponse{
		Status: wrapStatus(o.engine.Unsubscribe()),
	}, nil
}

func (o *onesecApi) Exit(_ context.Context, in *onesecpb.ExitRequest) (*onesecpb.ExitResponse, error) {
	o.exitCallback(in.StopImmediately)
	return &onesecpb.ExitResponse{}, nil
}
