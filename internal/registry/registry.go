package registry

import (
	"gitlab.com/karihirvi/onesec/internal/binance"
	"gitlab.com/karihirvi/onesec/internal/binance_todo"
	"gitlab.com/karihirvi/onesec/internal/database"
	"gitlab.com/karihirvi/onesec/internal/engine"
	"gitlab.com/karihirvi/onesec/internal/global"
	"gitlab.com/karihirvi/onesec/internal/server"
)

/*
Registry is a central hub for creating objects (=services) with constructor values taken from the configuration.
Clients are free to create service objects directly, but the registry provides default objects for convenience.
*/

// NewDefaultServer creates a new Server using values from the configuration.
func NewDefaultServer() *server.Server {
	cf := &server.Config{
		ServerAddress: global.Conf.General.ServerAddress,
		ExitSleep:     global.Conf.General.ExitSleep,
	}
	db := NewDefaultDatabase()
	return server.NewServer(cf, global.Log, db, NewDefaultEngine(db))
}

// NewDefaultEngine creates a new Engine using values from the configuration and
// the supplied database.
func NewDefaultEngine(dbService database.IDatabase) *engine.Engine {
	cf := &engine.Config{
		StoragePath:         global.Conf.Engine.StoragePath,
		StreamingBufferSize: global.Conf.Engine.StreamingBufferSize,
	}
	return engine.NewEngine(cf, global.Log, dbService, binance.NewDefaultStreamManager(),
		binance.NewDefaultStreamParser())
}

// NewDefaultDatabase create a new database using values from the configuration.
func NewDefaultDatabase() database.IDatabase {
	return database.NewDatabase(&database.Config{
		MongoUri:                           global.Conf.MongoDB.Uri,
		MongoTimeout:                       global.Conf.MongoDB.Timeout,
		StateCollectionSize:                global.Conf.General.StatesCollectionSize,
		OperationsCollectionSize:           global.Conf.General.OperationsCollectionSize,
		OnesecDbName:                       global.Conf.General.Database,
		MetaCollectionSize:                 global.Conf.General.MetaCollectionSize,
		SeriesCollectionExpireAfterSeconds: global.Conf.General.SeriesCollectionExpireAfterSeconds,
	})
}

func RunArbitrage() error {
	cf := &binance_todo.Config{
		ListenBufferSize:    global.Conf.ListenBuffer,
		TakerFee:            global.Conf.TakerFee,
		StatsRuntimeAverage: global.Conf.StatsRuntimeAverage,
	}
	return binance_todo.RunArbitrage(cf, global.Log, binance_todo.GetAvailablePairs())
}
