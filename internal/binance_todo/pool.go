package binance_todo

import (
	"github.com/shopspring/decimal"
	"gitlab.com/karihirvi/onesec/internal/bellmanford"
)

func NewAlgoPool(conf *Config, size int, pairs []AssetPair) *AlgoPool {
	p := &AlgoPool{}
	for i := 0; i < size; i++ {
		p.idles = append(p.idles, makeAssetGraph(conf, pairs))
	}
	return p
}

// AlgoPool is an algorithm pool
type AlgoPool struct {
	busys []*bellmanford.Graph
	idles []*bellmanford.Graph
}

func (a *AlgoPool) UpdateEdge(update bellmanford.SymbolUpdate) {

}

func makeAssetGraph(conf *Config, pairs []AssetPair) *bellmanford.Graph {
	var assets []string
	// helper
	addAsset := func(asset string) {
		for _, s := range assets {
			if s == asset {
				return
			}
		}
		assets = append(assets, asset)
	}
	// Find all distinct assets
	for _, pair := range pairs {
		addAsset(pair.BaseAsset)
		addAsset(pair.QuoteAsset)
	}
	vertices := make([]int, len(assets))
	assetToVertex := map[string]int{}
	var edges []*bellmanford.Edge
	for i, asset := range assets {
		vertices[i] = i // vertex ids are also vertex indices
		assetToVertex[asset] = i
	}
	symbolToEdge := map[string]bellmanford.EdgePair{}

	// Add buy and sell edges. The exchange offers assets as trade pairs BaseAsset/QuoteAsset, e.g., BTC/USDT.
	// 1) Buy edge means you're buying, i.e., buy BTC/USDT pair with USDT and get BTC.
	//    Since you're buying, it's the exchange's ask price that need to be applied, i.e., ask(BTC/USDT)
	//    The edge weight is log(ask(BTC/USDT))
	// 2) Sell edge is the inverse of the above. The rate in inverse (BTC/USDT)^-1 and since you're selling, it's
	//    the bid price you need to use, i.e., (bid(BTC/USDT))^-1.
	//    The edge weight is log((bid(BTC/USDT))^-1) = -log(bid(BTC/USDT))
	//
	// Bid and Ask prices are updated in such a manner that you can go from "From" asset to "To" asset
	// by multiplication.
	for _, pair := range pairs {
		edges = append(edges,
			&bellmanford.Edge{ // Buy BTC/USDT
				From:      assetToVertex[pair.QuoteAsset], // You have quote assets USDT to buy with
				To:        assetToVertex[pair.BaseAsset],  // and receive base assets BTC as the result.
				Symbol:    pair.Symbol(),                  // BTCUSDT
				Direction: bellmanford.Buy,
				Weight:    0,                 // Weight will be updated to log(ask(BTC/USDT))
				Bid:       decimal.Decimal{}, // Will NOT be updated
				Ask:       decimal.Decimal{}, // Will be updated to ask(BTC/USDT)
			},
			&bellmanford.Edge{ // Sell BTC/USDT
				From:      assetToVertex[pair.BaseAsset],  // You have base assets BTC which you sell
				To:        assetToVertex[pair.QuoteAsset], // and receive quote assets USDT as the result.
				Symbol:    pair.Symbol(),                  // BTCUSDT
				Direction: bellmanford.Sell,
				Weight:    0,                 // Weight will be updated to -log(bid(BTC/USDT))
				Bid:       decimal.Decimal{}, // Will be updated as bid(BTC/USDT)
				Ask:       decimal.Decimal{}, // Will NOT be updated
			},
		)
		symbolToEdge[pair.Symbol()] = bellmanford.EdgePair{
			BuyEdge:  edges[len(edges)-2],
			SellEdge: edges[len(edges)-1],
		}
	}
	return bellmanford.NewGraph(&bellmanford.Config{StatsRuntimeAverage: conf.StatsRuntimeAverage},
		edges, vertices, assets, symbolToEdge)
}
