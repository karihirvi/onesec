package binance_todo

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/shopspring/decimal"
	"gitlab.com/karihirvi/onesec/internal/bellmanford"
	"gitlab.com/karihirvi/onesec/internal/binance"
	"go.uber.org/atomic"
	"go.uber.org/zap"
	"strings"
	"time"
)

/*
Binance package implements all Binance exchange related services.
*/

// Avoid using floats and convert prices to integers using this multiplier, 10^8
// const priceMultiplier int64 = 100000000

type subscribeMsg struct {
	Method string   `json:"method"`
	Params []string `json:"params"`
	Id     int      `json:"id"`
}

type bookTicker struct {
	UpdateId int64  `json:"u"`
	Symbol   string `json:"s"`
	Bid      string `json:"b"`
	BidQty   string `json:"B"`
	Ask      string `json:"a"`
	AskQty   string `json:"A"`
}

type arbitrageLoop struct {
	vertices []int
}

type Config struct {
	ListenBufferSize    int
	TakerFee            decimal.Decimal
	StatsRuntimeAverage int
}

func RunArbitrage(conf *Config, log *zap.SugaredLogger, pairs []AssetPair) error {
	// Fill
	graph := makeAssetGraph(conf, pairs)
	var symbols []string
	for _, pair := range pairs {
		symbols = append(symbols, pair.Symbol())
	}
	ch, _, _, err := ListenPairs(conf, log, GetAvailablePairs())
	if err != nil {
		return err
	}
	// symbolCount := map[string]int64{}
	// allWeightsFilled := false
	start := time.Now()
	for update := range ch {
		if graph.UpdateEdge(update) {
			if loops := graph.FindArbitrageLoops(); loops != nil {
				for _, loop := range loops {
					factor, str := graph.CalculateTradeSequence(loop.Vertices)
					log.Infow("Arbitrage", "factor", factor, "sequence", str)
				}

			}
		}
		if time.Since(start) > 3*time.Second {
			stat := graph.GetStatistics()
			str := ""
			var totalCount int64
			for _, count := range stat.SymbolCount {
				totalCount += count
			}
			for symbol, count := range stat.SymbolCount {
				str += fmt.Sprintf("%v (%v, %.f) ", symbol, count, float64(count)/float64(totalCount))
			}
			str = strings.Trim(str, " ")
			log.Infow("Stats", "symbols", str, "runCount", stat.RunCount, "aveRuntime",
				stat.AverageRuntime)
			start = time.Now()
		}
	}
	return nil
}

func ListenPairs(conf *Config, log *zap.SugaredLogger, pairs []AssetPair) (
	<-chan bellmanford.SymbolUpdate, func(), <-chan struct{}, error) {
	url := fmt.Sprintf("%v/ws", binance.UrlBinanceWsBase)
	conn, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		return nil, nil, nil, err
	}
	updateCh := make(chan bellmanford.SymbolUpdate, conf.ListenBufferSize)
	msg := subscribeMsg{
		Method: "SUBSCRIBE",
		Id:     1,
	}
	for _, pair := range pairs {
		symbol := strings.ToLower(fmt.Sprintf("%v%v", pair.BaseAsset, pair.QuoteAsset))
		msg.Params = append(msg.Params, fmt.Sprintf("%v@bookTicker", symbol))
	}
	data, err2 := json.Marshal(msg)
	if err2 != nil {
		return nil, nil, nil, err2
	}
	if err3 := conn.WriteMessage(websocket.TextMessage, data); err3 != nil {
		return nil, nil, nil, err3
	}

	stopFlag := atomic.NewBool(false)
	stop := func() {
		stopFlag.Store(true)
		func() { _ = conn.Close() }()
	}
	stopped := make(chan struct{}, 1)
	go listenLoop(log, conf.TakerFee, conn, updateCh, stopFlag, stopped)
	return updateCh, stop, stopped, nil
}

func listenLoop(log *zap.SugaredLogger, takerFee decimal.Decimal, conn *websocket.Conn,
	updateCh chan bellmanford.SymbolUpdate, stopFlag *atomic.Bool, stopped chan struct{}) {
	//
	defer func() { stopped <- struct{}{} }()
	defer close(updateCh)
	defer func() { _ = conn.Close() }()
	countMap := map[string]int64{}
	// Fee percentage as a multiplier
	feeMul := takerFee.Div(decimal.New(100, 0))
	// Fee multipliers for bid and ask
	bidFeeMul := decimal.New(1, 0).Sub(feeMul)
	askFeeMul := decimal.New(1, 0).Add(feeMul)
	println(bidFeeMul.String())
	println(askFeeMul.String())
	for {
		_, msg2, err4 := conn.ReadMessage()
		if stopFlag.Load() {
			return
		}
		if err4 != nil {
			log.Warnw("websocket error", "error", err4)
			continue
		}
		bt := &bookTicker{}
		err5 := json.Unmarshal(msg2, bt)
		if err5 != nil {
			log.Warnw("unmarshal error", "error", err5)
			continue
		}
		if bt.UpdateId == 0 {
			continue
		}
		bid, err6 := decimal.NewFromString(bt.Bid)
		if err6 != nil {
			log.Warnw("error converting bid", "error", err6)
		}
		ask, err7 := decimal.NewFromString(bt.Ask)
		if err7 != nil {
			log.Warnw("error converting ask", "error", err7)
		}
		countMap[bt.Symbol]++
		select {
		case updateCh <- bellmanford.SymbolUpdate{
			Count:  countMap[bt.Symbol],
			Symbol: bt.Symbol,
			Bid:    bid.Mul(bidFeeMul),
			Ask:    ask.Mul(askFeeMul),
		}:
		default:
			log.Warnw("channel full")
		}
	}
}

// Open a websocket stream, https://binance-docs.github.io/apidocs/spot/en/#websocket-market-streams
// Returns a channel for receiving data. Client must read the channel continuously, or otherwise
// data wil be dropped.
func Open(streamName string) (chan *Message, func(), <-chan struct{}, error) {
	// panic("implement me")
	url := fmt.Sprintf("%v/ws/%v", binance.UrlBinanceWsBase, streamName)
	conn, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		return nil, nil, nil, err
	}
	ch := make(chan *Message, 1)
	stopFlag := atomic.NewBool(false)
	stop := func() {
		stopFlag.Store(true)
		func() { _ = conn.Close() }()
	}
	stopped := make(chan struct{}, 1)
	go func() {
		// Multiple defers are executed in LIFO order
		defer func() { stopped <- struct{}{} }()
		defer close(ch)
		defer func() { _ = conn.Close() }()

		var count int64
		for {
			_, msg, err2 := conn.ReadMessage()
			if stopFlag.Load() {
				return
			}
			count++
			ch <- &Message{
				Count: count,
				Data:  msg,
				Error: err2,
			}
			if err2 != nil {
				return
			}
		}
	}()
	return ch, stop, stopped, nil
}
