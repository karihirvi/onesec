package binance_todo

import (
	"strings"
)

type Message struct {
	Count int64
	Data  []byte
	Error error
}

const (
	Btc  = "btc"
	Usdt = "usdt"
	Eth  = "eth"
	Ltc  = "ltc"
	Xrp  = "xrp"
	Xlm  = "xlm"
	Bnb  = "bnb"
)

// AssetPair represents currency pair
type AssetPair struct {
	BaseAsset  string // Base asset
	QuoteAsset string // Quote asset
}

func (c *AssetPair) Symbol() string {
	return strings.ToUpper(c.BaseAsset + c.QuoteAsset)
}

func GetAvailablePairs() []AssetPair {
	p := func(base, quote string) AssetPair {
		return AssetPair{BaseAsset: base, QuoteAsset: quote}
	}
	return []AssetPair{p(Btc, Usdt), p(Eth, Usdt), p(Ltc, Usdt), p(Xrp, Usdt), p(Xlm, Usdt),
		p(Bnb, Usdt), p(Eth, Btc), p(Bnb, Btc), p(Ltc, Btc), p(Xrp, Btc), p(Xlm, Btc)}
}
