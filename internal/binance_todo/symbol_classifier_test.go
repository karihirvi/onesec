package binance_todo

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/karihirvi/onesec/internal/binance"
	"gitlab.com/karihirvi/onesec/internal/testutil"
	"testing"
)

func TestSymbolClassifier_FindLoops(t *testing.T) {
	exInfo := binance.NewExchangeInfo()
	require.NoError(t, exInfo.LoadFromFile(testutil.ExchangeInfoFile))
}
