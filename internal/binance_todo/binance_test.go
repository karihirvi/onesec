package binance_todo

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/karihirvi/onesec/internal/global"
	"gitlab.com/karihirvi/onesec/internal/testutil"
	"testing"
	"time"
)

func TestWebsocket(t *testing.T) {
	_, stop, stopped, err := Open("btcbusd@bookTicker2")
	require.Nil(t, err)
	time.Sleep(time.Second)
	stop()
	<-stopped
	// count := 0
	// for msg := range ch {
	//	println(string(msg.Data))
	//	count++
	//	if count == 10 {
	//		stop()
	//		<-stopped
	//		break
	//	}
	// }
}

func TestListenPairs(t *testing.T) {
	testutil.SetupProject(t)
	conf := &Config{
		ListenBufferSize:    global.Conf.ListenBuffer,
		TakerFee:            global.Conf.TakerFee,
		StatsRuntimeAverage: global.Conf.StatsRuntimeAverage,
	}
	ch, stop, stopped, err := ListenPairs(conf, global.Log, GetAvailablePairs())
	require.Nil(t, err)
	count := 0
	for update := range ch {
		global.Log.Infow("Received", "symbol", update.Symbol, "bid", update.Bid, "ask", update.Ask,
			"count", update.Count)
		count++
		if count == 20 {
			stop()
			<-stopped
			break
		}
	}
}
