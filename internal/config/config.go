package config

import (
	"fmt"
	"github.com/shopspring/decimal"
	"gopkg.in/yaml.v2"
	"os"
	"time"
)

/*
Config package implements yaml configuration reading. For documentation about each configuration key,
please refer to configs/onesec.yaml file.
*/

type Config struct {
	General `yaml:"general"`
	MongoDB `yaml:"mongodb"`
	Engine  `yaml:"engine"`
	Algo    `yaml:"algo"`
	Binance `yaml:"binance"`
	Logging `yaml:"logging"`
}

type General struct {
	ServerAddress                      string        `yaml:"server-address"`
	ExitSleep                          time.Duration `yaml:"exit-sleep-ms"`
	Database                           string        `yaml:"database"`
	StatesCollectionSize               int64         `yaml:"states-collection-size-mb"`
	OperationsCollectionSize           int64         `yaml:"operations-collection-size-mb"`
	MetaCollectionSize                 int64         `yaml:"meta-collection-size-mb"`
	SeriesCollectionExpireAfterSeconds int64         `yaml:"series-collection-expire-after-seconds"`
}

type MongoDB struct {
	Uri     string        `yaml:"uri"`
	Timeout time.Duration `yaml:"timeout-ms"`
}

type Engine struct {
	StoragePath         string `yaml:"storage-path"`
	StreamingBufferSize int    `yaml:"streaming-buffer-size"`
}

type Algo struct {
	ListenBuffer        int `yaml:"listen-buffer"`
	PoolSize            int `yaml:"pool-size"`
	StatsRuntimeAverage int `yaml:"stats-runtime-average"`
}

type Binance struct {
	Credentials string          `yaml:"credentials"`
	TakerFee    decimal.Decimal `yaml:"taker-fee"`
}

type Logging struct {
	Production         bool          `yaml:"production"`
	AlgoStatus         bool          `yaml:"algo-status"`
	AlgoStatusInterval time.Duration `yaml:"algo-status-interval"`
}

// ReadConfig accepts a list of yaml configuration files, reads and applies them one after the other
// with later values overwriting the previous ones. Variables of form ${var} or $var are replaced
// by the corresponding environment variable values.
func ReadConfig(configPaths []string) (*Config, error) {
	conf := &Config{}
	for _, path := range configPaths {
		data, err := os.ReadFile(path)
		if err != nil {
			return nil, fmt.Errorf("error reading config file from '%s', error: %w", path, err)
		}
		data = []byte(os.ExpandEnv(string(data)))
		err2 := yaml.Unmarshal(data, conf)
		if err2 != nil {
			return nil, failedToUnmarshal(err2)
		}
	}
	convertFields(conf)
	return conf, nil
}

func failedToUnmarshal(err error) error {
	return fmt.Errorf("error unmarshaling config file, error: %w", err)
}

// convertFields modifies selected fields for easier programmatic access.
func convertFields(c *Config) {
	var toBytes int64 = 1024 * 1024
	// General
	c.General.ExitSleep *= time.Millisecond
	c.General.StatesCollectionSize *= toBytes
	c.General.OperationsCollectionSize *= toBytes
	c.General.MetaCollectionSize *= toBytes
	// Mongo
	c.MongoDB.Timeout *= time.Millisecond
	// Logging
	c.Logging.AlgoStatusInterval *= time.Second
}

// Credentials for Binance.
type Credentials struct {
	ApiKey    string `yaml:"api-key"` // Api key
	SecretKey string `yaml:"secret-key"`
}

// ReadCredentials reads credentials from a file.
func ReadCredentials(path string) (*Credentials, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("error reading credentials from '%s', error: %w", path, err)
	}
	cred := &Credentials{}
	if err2 := yaml.Unmarshal(data, cred); err2 != nil {
		return nil, failedToUnmarshal(err2)
	}
	return cred, nil
}
