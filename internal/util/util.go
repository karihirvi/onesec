package util

import (
	"log"
	"path"
	"runtime"
)

/*
Util package contains various commonly used utilities.
*/

// RepoRoot Path to the root of the repo, used in testing
var RepoRoot string

// https://brandur.org/fragments/testing-go-project-root
func init() {
	_, filename, _, _ := runtime.Caller(0)
	RepoRoot = path.Clean(path.Join(path.Dir(filename), "../.."))
}

// LogPanicOnError Use this if you want to stop executing in case of an error
func LogPanicOnError(err error) {
	if err != nil {
		log.Panic(err.Error())
	}
}

// PanicOnError panics if there's an error. Used for coverage.
func PanicOnError(err error) {
	if err != nil {
		panic(err)
	}
}
