package util

import (
	"errors"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestPanicOnError(t *testing.T) {
	require.Panics(t, func() {
		PanicOnError(errors.New("some error"))
	})
}

func TestLogPanicOnError(t *testing.T) {
	require.Panics(t, func() {
		PanicOnError(errors.New("some error"))
	})
}
