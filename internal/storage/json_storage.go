package storage

import (
	"encoding/json"
	"fmt"
	"os"
)

// Default file permissions.
const filePermissions os.FileMode = 0644

// Default indentation when saving JSON to a file.
const jsonIndent = "  "

func NewJsonStorage() *JsonStorage {
	return &JsonStorage{}
}

type JsonStorage struct {
	JsonData []byte
}

// Hook required for testing
var convertToPrettyJsonTestHook func(info *interface{})

func convertToPrettyJson(oneLineJson []byte) (prettyJson []byte, err error) {
	var info interface{}
	if err = json.Unmarshal(oneLineJson, &info); err != nil {
		return nil, fmt.Errorf("error unmarshaling JSON: %w", err)
	}
	// Testing.
	if convertToPrettyJsonTestHook != nil {
		convertToPrettyJsonTestHook(&info)
	}
	prettyJson, err = json.MarshalIndent(info, "", jsonIndent)
	if err != nil {
		return nil, fmt.Errorf("error marshaling JSON: %w", err)
	}
	return
}

// SaveToFile saves exchange info to a file in JSON format.
func (j *JsonStorage) SaveToFile(filepath string, pretty bool) (err error) {
	out := j.JsonData
	if pretty {
		if out, err = convertToPrettyJson(j.JsonData); err != nil {
			return fmt.Errorf("error converting JSON to a pretty format: %w", err)
		}
	}
	if err = os.WriteFile(filepath, out, filePermissions); err != nil {
		return fmt.Errorf("error writing exchange info to a file '%s': %w", filepath, err)
	}
	return
}

// LoadFromFile loads exchange info from a JSON file.
func (j *JsonStorage) LoadFromFile(filepath string) (err error) {
	j.JsonData, err = os.ReadFile(filepath)
	if err != nil {
		return fmt.Errorf("error reading exchange info from a file '%s': %w", filepath, err)
	}
	return
}
