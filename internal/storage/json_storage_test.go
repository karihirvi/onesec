package storage

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/karihirvi/onesec/internal/testutil"
	"os"
	"testing"
)

var jsonBytes = []byte(`{"stream":"btcusdt@bookTicker","data":{"u":37510070060,"s":"BTCUSDT","b":"30073.53000000",
"B":"7.07702000","a":"30073.54000000","A":"0.13970000"}}`)

func TestJsonStorage_LoadFromFile(t *testing.T) {
	jsonStorage := NewJsonStorage()
	require.NoError(t, jsonStorage.LoadFromFile(testutil.ExchangeInfoFile))
	require.ErrorContains(t, jsonStorage.LoadFromFile(testutil.ExchangeInfoFile+"badPath"), "error reading exchange info "+
		"from a file '/Users/karihirvi/repos/onesec/test/testdata/exchange_info.jsonbadPath': open")
}

func saveToFileTest(t *testing.T, jsonStorage *JsonStorage, pretty bool, refFile string) {
	require.NoError(t, jsonStorage.LoadFromFile(refFile))
	// Save to a temporary file.
	file, err := os.CreateTemp("", "exchange-info-*")
	require.NoError(t, err)
	defer func() {
		require.NoError(t, os.Remove(file.Name()))
	}()
	require.NoError(t, jsonStorage.SaveToFile(file.Name(), pretty))
	// Check that it's ok by reading back and comparing to the original
	var savedData []byte
	savedData, err = os.ReadFile(file.Name())
	require.NoError(t, err)
	require.NoError(t, err)
	require.Equal(t, jsonStorage.JsonData, savedData)
}

func TestJsonStorage_SaveToFile(t *testing.T) {
	saveToFileTest(t, NewJsonStorage(), false, testutil.ExchangeInfoFile)
}

func TestJsonStorage_SaveToFile2(t *testing.T) {
	saveToFileTest(t, NewJsonStorage(), true, testutil.ExchangeInfoFile)
}

func TestJsonStorage_SaveToFile3(t *testing.T) {
	jsonStorage := NewJsonStorage()
	jsonStorage.JsonData = []byte("bad json")
	// Pretty JSON unmarshal error.
	require.ErrorContains(t, jsonStorage.SaveToFile("dummy", true), "error converting JSON to a pretty format: "+
		"error unmarshaling JSON: invalid character 'b' looking for beginning of value")
	// Pretty JSON marshal error.
	require.NoError(t, jsonStorage.LoadFromFile(testutil.ExchangeInfoFile))
	convertToPrettyJsonTestHook = func(info *interface{}) {
		((*info).(map[string]interface{}))["some key"] = make(chan struct{}) // Bad value
	}
	require.ErrorContains(t, jsonStorage.SaveToFile("dummy2", true), "error converting JSON to a pretty format: "+
		"error marshaling JSON: json: unsupported type: chan struct {}")
}

func TestJsonStorage_SaveToFile4(t *testing.T) {
	jsonStorage := NewJsonStorage()
	require.NoError(t, jsonStorage.LoadFromFile(testutil.ExchangeInfoFile))
	// Save to a temporary file fails.
	file, err := os.CreateTemp("", "exchange-info-*")
	require.NoError(t, err)
	defer func() {
		require.NoError(t, os.Remove(file.Name()))
	}()
	require.NoError(t, os.Chmod(file.Name(), 0000))
	require.ErrorContains(t, jsonStorage.SaveToFile(file.Name(), false), "error writing exchange info to a file")
	require.NoError(t, os.Chmod(file.Name(), 0600))
}
