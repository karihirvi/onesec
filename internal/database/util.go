package database

import (
	"fmt"
	"github.com/shopspring/decimal"
	"gitlab.com/karihirvi/onesec/internal/util"
	"go.mongodb.org/mongo-driver/bson/bsoncodec"
	"go.mongodb.org/mongo-driver/bson/bsonrw"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"reflect"
)

// DecimalCodec is a ValueCodec that allows encoding decimal.Decimal to primitive.Decimal128 and decoding
// primitive.Decimal128 to decimal.Decimal.
type DecimalCodec struct{}

var _ bsoncodec.ValueCodec = &DecimalCodec{}

func (dc *DecimalCodec) EncodeValue(_ bsoncodec.EncodeContext, vw bsonrw.ValueWriter, val reflect.Value) error {
	// Use reflection to convert the reflect.Value to decimal.Decimal.
	dec, ok := val.Interface().(decimal.Decimal)
	panicOnFalse(ok, fmt.Errorf("value %v to encode is not of type decimal.Decimal", val))

	// Convert decimal.Decimal to primitive.Decimal128.
	primDec, err := primitive.ParseDecimal128(dec.String())
	util.PanicOnError(err)

	return vw.WriteDecimal128(primDec)
}

func (dc *DecimalCodec) DecodeValue(_ bsoncodec.DecodeContext, vr bsonrw.ValueReader, val reflect.Value) error {
	// Read primitive.Decimal128 from the ValueReader.
	primDec, err := vr.ReadDecimal128()
	util.PanicOnError(err)

	// Convert primitive.Decimal128 to decimal.Decimal.
	dec, err2 := decimal.NewFromString(primDec.String())
	util.PanicOnError(err2)

	// Set val to the decimal.Decimal value contained in dec.
	val.Set(reflect.ValueOf(dec))
	return nil
}

// panicOnFalse panics if the boolean is false. Used for coverage.
func panicOnFalse(ok bool, err error) {
	if !ok {
		panic(err)
	}
}
