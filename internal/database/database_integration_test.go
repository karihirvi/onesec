//go:build integration_test

package database

import (
	"context"
	"errors"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/require"
	"gitlab.com/karihirvi/onesec/internal/util"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/exp/rand"
	"os"
	"path"
	"testing"
	"time"
)

func removeTestDatabases(t *testing.T, dbSrv *databaseImpl) {
	require.NoError(t, dbSrv.db.Drop(context.Background()))
	require.NoError(t, dbSrv.db.Drop(context.Background()))
}

func TestDatabase_Connect(t *testing.T) {
	conf := makeTestConfig()
	// Ok
	db := newDatabaseIMongo(conf, &iMongoImpl{})
	require.NoError(t, db.Connect())
	require.NoError(t, db.Initialize())
	removeTestDatabases(t, db)
	// Failure
	conf.MongoUri += "bad"
	db = newDatabaseIMongo(conf, &iMongoImpl{})
	require.ErrorContains(t, db.Connect(), "error connecting to MongoDB with URI 'mongodb://localhost:27017bad': "+
		"error parsing uri: invalid host")
}
func TestDatabase_Disconnect(t *testing.T) {
	conf := makeTestConfig()
	db := newDatabaseIMongo(conf, &iMongoImpl{})
	// No client case.
	require.NoError(t, db.Disconnect())
	// With client
	require.NoError(t, db.Connect())
	require.NoError(t, db.Disconnect())
}

func makeRandomDecimal() decimal.Decimal {
	return decimal.NewFromFloat(rand.Float64() * 1e6)
}

type TestDecimal struct {
	Timestamp time.Time       `bson:"timestamp"`
	Value     decimal.Decimal `bson:"value,omitempty"`
}

func makeTestDatabases(t *testing.T) *databaseImpl {
	conf := makeTestConfig()
	db := NewDatabase(conf).(*databaseImpl)
	require.NoError(t, db.Connect())
	require.NoError(t, db.Initialize())
	return db
}

func TestDecimalCodec_EncodeValue(t *testing.T) {
	dbSrv := makeTestDatabases(t)
	defer removeTestDatabases(t, dbSrv)
	// All ok
	now := time.Now().UTC().Round(time.Millisecond)
	valRef := TestDecimal{Timestamp: now, Value: decimal.NewFromFloat(1234.56789)}
	ctx, cancel := context.WithTimeout(context.Background(), testTimeout)
	defer cancel()
	col := dbSrv.db.Collection(kBookTickerCollection)
	result, err := col.InsertOne(ctx, valRef)
	require.NoError(t, err)
	id := result.InsertedID.(primitive.ObjectID)
	var val TestDecimal
	require.NoError(t, col.FindOne(ctx, bson.D{{"_id", id}}).Decode(&val))
	require.Equal(t, valRef, val)
}

func TestPanicOnFalse(t *testing.T) {
	require.Panics(t, func() {
		panicOnFalse(false, errors.New("some error"))
	})
}

func loadTestJsonFromFile(t *testing.T) []byte {
	filepath := path.Join(util.RepoRoot, "test", "testdata", "exchange_info.json")
	jsonBytes, err := os.ReadFile(filepath)
	require.NoError(t, err)
	return jsonBytes
}

func TestDatabaseImpl_SaveExchangeInfo(t *testing.T) {
	db := makeTestDatabases(t)
	defer removeTestDatabases(t, db)
	refJson := loadTestJsonFromFile(t)
	require.NoError(t, db.SaveExchangeInfo(refJson))
	// Load it back
	loadedJson, whenSaved, err := db.LoadExchangeInfo()
	require.NoError(t, err)
	// print(whenSaved)
	require.Equal(t, refJson, loadedJson)
	// Add second
	dummy := []byte("JSON is saves as bytes so we can simulate it by a string")
	require.NoError(t, db.SaveExchangeInfo(dummy))
	loadedDummy, whenSaved2, err2 := db.LoadExchangeInfo()
	require.NoError(t, err2)
	require.Equal(t, dummy, loadedDummy)
	require.True(t, whenSaved2.After(whenSaved))
}

// func TestDecimalCodec_EncodeValue2(t *testing.T) {
//     conf := makeTestConfig()
//     db := newDatabaseIMongo(conf, &iMongoImpl{})
//     require.NoError(t, db.Connect())
//     require.NoError(t, db.Initialize())
//     defer removeTestDatabases(t, db)
//
//     binTickerRef := BinanceTicker{
//         Ticker:               Ticker{Timestamp: time.Now()},
//         EventType:            "event type",
//         EventTime:            1234,
//         Symbol:               "symbol",
//         PriceChange:          decimal.NewFromFloat(12345.6789),
//         PriceChangePercent:   makeRandomDecimal(),
//         WeightedAveragePrice: makeRandomDecimal(),
//         FirstTradePrice:      makeRandomDecimal(),
//         LastPrice:            makeRandomDecimal(),
//         LastQuantity:         12,
//         BestBidPrice:         makeRandomDecimal(),
//         BestBidQuantity:      23,
//         BestAskPrice:         makeRandomDecimal(),
//         BestAskQuantity:      makeRandomDecimal(),
//         OpenPrice:            makeRandomDecimal(),
//         HighPrice:            makeRandomDecimal(),
//         LowPrice:             makeRandomDecimal(),
//         BaseAssetVolume:      34,
//         QuoteAssetVolume:     45,
//         FirstTradeId:         56,
//         LastTradeId:          67,
//         NumberOfTrades:       78,
//     }
//     ctx, cancel := context.WithTimeout(context.Background(), testTimeout)
//     defer cancel()
//     col := db.binanceDb.Collection(kSeriesCollection)
//     result, err := col.InsertOne(ctx, binTickerRef)
//     require.NoError(t, err)
//     id := result.InsertedID.(primitive.ObjectID)
//     var binTicker BinanceTicker
//     require.NoError(t, col.FindOne(ctx, bson.D{{"_id", id}}).Decode(&binTicker))
//     require.Equal(t, binTickerRef, binTickerRef)
// }
