package database

import (
	"time"
)

// DocumentType defines document type stored in a MongoDB collection.
type DocumentType int32

const (
	DocTypeExchangeInfo DocumentType = iota + 1 // Exchange info.
	DocTypeTicker
	DocTypeBookTicker
)

const (
	docTypeField      = "documentType"
	docTimestampField = "timestamp"
)

// Document describes MongoDB document metadata, e.g., type. Document is embedded in all structs
// that are saved to MongoDB.
type Document struct {
	// Id   primitive.ObjectID `bson:"_id"` // Id
	Type DocumentType `bson:"documentType"`
}

// State describes persistent state stored in a database.
type State struct {
	Document     `bson:",inline"`
	CreatedTime  time.Time `bson:"createdTime"`  // Created time
	ModifiedTime time.Time `bson:"modifiedTime"` // Modified time
}

// BinanceExchangeInfo contains Binance exchange info in JSON format.
type BinanceExchangeInfo struct {
	Document  `bson:",inline"`
	Timestamp time.Time `bson:"timestamp"` // When saved.
	Json      []byte    `bson:"json"`      // Exchange info JSON in bytes.
}
