package database

import (
	"context"
	"fmt"
	"github.com/shopspring/decimal"
	"gitlab.com/karihirvi/onesec/internal/binance"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/exp/slices"
	"reflect"
	"sync"
	"time"
)

/*
Database package handles all database (i.e. MongoDB) related services.
*/

// Fixed MongoDB collection names.
const (
	kStatesCollection            = "states"             // Onesec state history.
	kOperationsCollection        = "operations"         // Onesec operation history, e.g, buy, sell, etc.
	kBookTickerCollection        = "booktickers"        // Book ticker time series.
	kTradeCollection             = "trades"             // Trade time series.
	kRollingWindowStatCollection = "rollingwindowstats" // Rolling window time series.
	// kSeriesCollection     = "series"     // Asset pricing time series.
	kMetaCollection = "meta" // Metadata, e.g., exchange infos.
)

// Config contains Server related configuration.
type Config struct {
	MongoUri                           string        // Connection URI to MongoDB.
	MongoTimeout                       time.Duration // MongoDB timeout.
	StateCollectionSize                int64         // 'state' collection size.
	OperationsCollectionSize           int64         // 'operations' collection size.
	MetaCollectionSize                 int64         // 'meta' collection size.
	SeriesCollectionExpireAfterSeconds int64         // 'series' expiration.
	OnesecDbName                       string        // Onesec related database.
}

func NewDatabase(conf *Config) IDatabase {
	return newDatabaseIMongo(conf, &iMongoImpl{})
}

func newDatabaseIMongo(conf *Config, iMongo iMongo) *databaseImpl {
	return &databaseImpl{conf: conf, iMongo: iMongo}
}

// Database implements IDatabase.
type databaseImpl struct {
	mux    sync.Mutex      // Serialise access to this object.
	conf   *Config         // Configuration
	client *mongo.Client   // MongoDB client
	db     *mongo.Database // Onesec database
	iMongo iMongo          // Wrapper interface for Mongo driver functions.
}

func (m *databaseImpl) SaveTrade(trade *binance.Trade) error {
	coll := m.db.Collection(kTradeCollection)

	_, err := coll.InsertOne(context.TODO(), trade)
	return err
}

func (m *databaseImpl) SaveRollingWindowStat(window *binance.RollingWindowStat) error {
	coll := m.db.Collection(kRollingWindowStatCollection)

	_, err := coll.InsertOne(context.TODO(), window)
	return err
}

func (m *databaseImpl) SaveBookTicker(ticker *binance.BookTicker) error {
	coll := m.db.Collection(kBookTickerCollection)

	_, err := coll.InsertOne(context.TODO(), ticker)
	return err
}

// Connect opens a database server connection.
func (m *databaseImpl) Connect() error {
	m.mux.Lock()
	defer m.mux.Unlock()
	ctx, cancel := context.WithTimeout(context.Background(), m.conf.MongoTimeout)
	defer cancel()
	// Creating a MongoDB client with custom decimal type handling.
	// See https://jira.mongodb.org/browse/GODRIVER-2244
	opts := options.Client().SetRegistry(
		bson.NewRegistryBuilder().RegisterCodec(
			reflect.TypeOf(decimal.Decimal{}),
			&DecimalCodec{},
		).Build()).ApplyURI(m.conf.MongoUri)
	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		return fmt.Errorf("error connecting to MongoDB with URI '%s': %w",
			m.conf.MongoUri, err)
	}
	m.client = client
	// Get references to databases
	m.db = client.Database(m.conf.OnesecDbName)
	return nil
}

// Disconnect from the database.
func (m *databaseImpl) Disconnect() error {
	m.mux.Lock()
	defer m.mux.Unlock()
	if m.client == nil {
		return nil
	}
	ctx, cancel := context.WithTimeout(context.Background(), m.conf.MongoTimeout)
	defer cancel()
	err := m.client.Disconnect(ctx)
	m.client = nil
	return err
}

// Initialize database connection and collections.
func (m *databaseImpl) Initialize() error {
	m.mux.Lock()
	defer m.mux.Unlock()
	if m.db == nil {
		return fmt.Errorf("database is not connected")
	}
	return m.initializeCollections(m.db, []*collectionsSpec{
		{
			name: kStatesCollection,
			opts: []*options.CreateCollectionOptions{
				options.CreateCollection().SetCapped(true).SetSizeInBytes(m.conf.StateCollectionSize)},
		}, {
			name: kOperationsCollection,
			opts: []*options.CreateCollectionOptions{
				options.CreateCollection().SetCapped(true).SetSizeInBytes(m.conf.OperationsCollectionSize)},
		}, {
			name: kMetaCollection,
			opts: []*options.CreateCollectionOptions{
				options.CreateCollection().SetCapped(true).SetSizeInBytes(m.conf.MetaCollectionSize)},
		}, {
			name: kBookTickerCollection,
			opts: []*options.CreateCollectionOptions{
				options.CreateCollection().SetTimeSeriesOptions(
					options.TimeSeries().SetTimeField("timestamp").SetMetaField("symbol")).
					SetExpireAfterSeconds(m.conf.SeriesCollectionExpireAfterSeconds)},
		}, {
			name: kTradeCollection,
			opts: []*options.CreateCollectionOptions{
				options.CreateCollection().SetTimeSeriesOptions(
					options.TimeSeries().SetTimeField("timestamp").SetMetaField("event_type")).
					SetExpireAfterSeconds(m.conf.SeriesCollectionExpireAfterSeconds)},
		}, {
			name: kRollingWindowStatCollection,
			opts: []*options.CreateCollectionOptions{
				options.CreateCollection().SetTimeSeriesOptions(
					options.TimeSeries().SetTimeField("timestamp").SetMetaField("event_type")).
					SetExpireAfterSeconds(m.conf.SeriesCollectionExpireAfterSeconds)},
		},
	}, m.conf.MongoTimeout)
}

// collectionsSpec defines collections to be created.
type collectionsSpec struct {
	name string                             // Collection name.
	opts []*options.CreateCollectionOptions // Options
}

// initializeCollections creates collections, if they don't exist already.
func (m *databaseImpl) initializeCollections(db *mongo.Database, specs []*collectionsSpec,
	timeout time.Duration) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	// Get existing collections.
	namesInDb, err := m.iMongo.ListCollectionNames(db, ctx, bson.D{})
	if err != nil {
		return fmt.Errorf("error listing collection names: %w", err)
	}
	// Create missing collections.
	for _, spec := range specs {
		if slices.Contains(namesInDb, spec.name) {
			continue
		}
		if err = m.iMongo.CreateCollection(db, ctx, spec.name, spec.opts...); err != nil {
			return fmt.Errorf("error creating a capped collection '%s': %w", spec.name, err)
		}
	}
	return nil
}

// SaveExchangeInfo saves exchange info and a timestamp of the save time.
func (m *databaseImpl) SaveExchangeInfo(infoJson []byte) error {
	ctx, cancel := context.WithTimeout(context.Background(), m.conf.MongoTimeout)
	defer cancel()
	_, err := m.db.Collection(kMetaCollection).InsertOne(ctx, BinanceExchangeInfo{
		Document:  Document{Type: DocTypeExchangeInfo},
		Timestamp: time.Now(),
		Json:      infoJson,
	})
	return err
}

// LoadExchangeInfo loads a previously save exchange info with timestamp. If there's no exchange info,
// the returned infoJson is nil and whenSaved is invalid.
func (m *databaseImpl) LoadExchangeInfo() (infoJson []byte, whenSaved time.Time, _ error) {
	ctx, cancel := context.WithTimeout(context.Background(), m.conf.MongoTimeout)
	defer cancel()
	var result BinanceExchangeInfo
	err := m.db.Collection(kMetaCollection).FindOne(ctx, bson.D{{docTypeField, DocTypeExchangeInfo}},
		options.FindOne().SetSort(bson.D{{docTimestampField, -1}})).Decode(&result)
	if err == mongo.ErrNoDocuments {
		return nil, time.Time{}, nil
	}
	if err != nil {
		return nil, time.Time{}, fmt.Errorf("error in querying exhange info: %w", err)
	}
	return result.Json, result.Timestamp, nil
}
