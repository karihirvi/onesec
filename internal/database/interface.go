package database

import (
	"context"
	"gitlab.com/karihirvi/onesec/internal/binance"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

// IDatabase is the interface that wraps a database service.
type IDatabase interface {
	Connect() error
	Initialize() error
	Disconnect() error
	SaveBookTicker(ticker *binance.BookTicker) error
	SaveTrade(trade *binance.Trade) error
	SaveRollingWindowStat(window *binance.RollingWindowStat) error
	SaveExchangeInfo(infoJson []byte) error
	LoadExchangeInfo() (infoJson []byte, whenSaved time.Time, _ error)
}

// *****************************************************************************
// Internal interfaces
// *****************************************************************************

// iMongo is the interface that wraps Mongo driver functions which need to be mocked for testing purposes.
type iMongo interface {
	// ListCollectionNames wraps mongo.Database.ListCollectionNames.
	ListCollectionNames(db *mongo.Database, ctx context.Context, filter interface{},
		opts ...*options.ListCollectionsOptions) ([]string, error)
	// CreateCollection wraps mongo.Database.CreateCollection.
	CreateCollection(db *mongo.Database, ctx context.Context, name string,
		opts ...*options.CreateCollectionOptions) error
	// RunCommand wraps mongo.Database.RunCommand
	RunCommand(db *mongo.Database, ctx context.Context, runCommand interface{},
		opts ...*options.RunCmdOptions) *mongo.SingleResult
}

// iMongoImpl implements wrappers.
type iMongoImpl struct {
}

func (i iMongoImpl) RunCommand(db *mongo.Database, ctx context.Context, runCommand interface{},
	opts ...*options.RunCmdOptions) *mongo.SingleResult {
	return db.RunCommand(ctx, runCommand, opts...)
}

// CreateCollection wraps mongo.Database.CreateCollection.
func (i iMongoImpl) CreateCollection(db *mongo.Database, ctx context.Context, name string,
	opts ...*options.CreateCollectionOptions) error {
	return db.CreateCollection(ctx, name, opts...)
}

// ListCollectionNames wraps mongo.Database.ListCollectionNames.
func (i iMongoImpl) ListCollectionNames(db *mongo.Database, ctx context.Context, filter interface{},
	opts ...*options.ListCollectionsOptions) ([]string, error) {
	return db.ListCollectionNames(ctx, filter, opts...)
}
