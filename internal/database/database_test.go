package database

import (
	"context"
	"fmt"
	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"testing"
	"time"
)

const testCollectionSize = 30 * 1024 * 1024

const testTimeout = time.Second

func makeTestConfig() *Config {
	return &Config{
		MongoUri:                 "mongodb://localhost:27017",
		MongoTimeout:             3 * time.Second,
		StateCollectionSize:      testCollectionSize,
		OperationsCollectionSize: testCollectionSize,
		OnesecDbName:             "onesec_test_" + ksuid.New().String(),
		MetaCollectionSize:       testCollectionSize,
	}
}

func makeMockDb() (*databaseImpl, *iMongoMock) {
	mck := &iMongoMock{}
	return newDatabaseIMongo(makeTestConfig(), mck), mck
}

func TestDatabase_Initialize(t *testing.T) {
	db, mck := makeMockDb()
	// Connect error
	require.ErrorContains(t, db.Initialize(), "database is not connected")
	mck.On("ListCollectionNames").Return(
		[]string{"collection1-1", "collection-2"}, nil).
		On("CreateCollection").Return(nil).
		On("RunCommand").Return(mongo.NewSingleResultFromDocument(struct{}{}, nil, nil))
	require.NoError(t, db.Connect())
	require.NoError(t, db.Initialize())
}

func TestDatabase_Initialize2(t *testing.T) {
	// Collection listing error
	db, mck := makeMockDb()
	require.NoError(t, db.Connect())
	mck.On("ListCollectionNames").Return([]string{}, fmt.Errorf("list collection error"))
	require.ErrorContains(t, db.Initialize(), "list collection error")
}

func TestDatabase_Initialize3(t *testing.T) {
	// Create collections error
	db, mck := makeMockDb()
	require.NoError(t, db.Connect())
	mck.On("ListCollectionNames").Return([]string{"collection1-1", "collection-2"}, nil).
		On("CreateCollection").Return(fmt.Errorf("create collection error"))
	require.ErrorContains(t, db.Initialize(), "create collection error")
}

// *******************************************************''

type iMongoMock struct {
	mock.Mock
}

func (i *iMongoMock) RunCommand(db *mongo.Database, ctx context.Context,
	runCommand interface{}, opts ...*options.RunCmdOptions) *mongo.SingleResult {
	args := i.Called()
	return args[0].(*mongo.SingleResult)
}

func (i *iMongoMock) ListCollectionNames(db *mongo.Database, ctx context.Context, filter interface{},
	opts ...*options.ListCollectionsOptions) ([]string, error) {
	args := i.Called()
	return args[0].([]string), args.Error(1)
}

func (i *iMongoMock) CreateCollection(db *mongo.Database, ctx context.Context, name string,
	opts ...*options.CreateCollectionOptions) error {
	args := i.Called()
	return args.Error(0)
}
