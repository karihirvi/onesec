#!/usr/bin/env sh

# See https://dev.to/talalyousif/excluding-files-from-code-coverage-in-go-291f

while read p || [ -n "$p" ]
do
sed -i '' "/${p//\//\\/}/d" ./coverage.txt
done < ./exclude-from-code-coverage.txt