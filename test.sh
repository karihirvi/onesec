#!/usr/bin/env sh

# Usage
#   1) Run unit tests: ./test.sh
#   2) Run unit tests with coverage: ./test.sh coverage
#   3) Run unit and integration tests: ./test.sh integration
#      - Requires that a local MongoDB server is set up.
#   4) Run unit and integration tests with coverage: ./test.sh integration coverage

TAGS="test"
COVER=""
for var in "$@"
do
  if [ "$var" = "integration" ]; then
      TAGS="$TAGS,integration_test"
  fi
  if [ "$var" = "coverage" ]; then
    COVER="-coverpkg=./... -coverprofile=coverage.txt -covermode=atomic"
  fi
done

go test  -tags=$TAGS $COVER -race ./...

if [ "$COVER" != "" ]; then
  ./exclude-from-code-coverage.sh
  go tool cover -func coverage.txt | grep total:
fi