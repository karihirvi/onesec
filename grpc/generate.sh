#!/usr/bin/env sh

# Generate protobuf code.
# NOTE: For Python, make sure your 'grpc' virtualenv is active and set up for generation.

OUT_GO=./pb/go/onesecpb
OUT_PYTHON=./pb/python/onesecpb

mkdir -p ${OUT_GO} ${OUT_PYTHON}

# Generate Go code.
protoc -I./protos  ./protos/*.proto --go_opt=paths=source_relative --go_out=${OUT_GO} \
  --go-grpc_out=${OUT_GO} --go-grpc_opt=paths=source_relative
# Create go.mod to pb directory.
cd pb/go || exit
if [ ! -f go.mod ]; then
  go mod init gitlab.com/karihirvi/onesec/pb
fi
go mod tidy
cd ../.. || exit

# Generate Python code.
python -m grpc_tools.protoc -I./protos --python_out=${OUT_PYTHON} --grpc_python_out=${OUT_PYTHON}  protos/*.proto