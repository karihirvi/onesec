# Project scope

Onesec project is about enabling continuity trading for everyone. If everyone had the perfect market information and 
access to the market, asset prices would be in equilibrium all the time. Of course, this is not the case in real l
ife and we have discrepancies in asset pricing, i.e., pricing errors. These errors usually last only for a short 
time until someone notices them, makes a quick buck by swift trading and the equilibrium is restored. 
A pricing error, or discontinuity, may also occur when new information comes publicly available. In the light 
of the new information, investors anticipate changes in asset prices, start trading, and thus realising the price 
changes. Again, this news based pricing error lasts only for a short time. For the lack of a better word, 
we'll call these as *continuity errors*, i.e., situations when asset prices are not in equilibrium with respect 
to each other and available information. And a trading strategy that aims to utilise continuity errors 
is called *continuity trading*.

Continuity error opportunities we're interested in this project are
1) Arbitrage
    - Cryptocurrency pairs in Binance.
    - Arbitrage across multiple cryptocurrency exchanges.
2) [Recurring news](https://www.investopedia.com/articles/active-trading/111313/how-trade-news.asp)
    - Interim results from companies.
    - Various indicators for a national economy, e.g., U.S. jobs report.

Technically, taking advantage of these errors requires different approaches. Arbitrage opportunities are detected 
by connecting to an exchange's API and trying to find arbitrage loops in currency pairs and trade them, all in 
real time. Transaction costs needs to be included and network latency has to be taken into account. Arbitrage 
loops can be run in a distributed manner. A master node reads Binance assets prices, forwards them to slave nodes 
which each has a subset of asset to consider, i.e., a big parameter space is chunked into smaller one. An other 
option is to have N parallel instance running Bellman-Ford arbitrage algorithm, each starting from a random asset 
pair and continuously feeding the best arbitrage loop found so far to the master node, which can decide to trade 
before all nodes are done.

Recurring news trading is based on predictable market reactions and you need to set triggers in advance what to
do when thresholds are exceeded. Recurring news are released at known times. Few minutes before the release, 
Onesec would start polling the website. Once the information is released, Onesec parses the website and/or PDFs 
for trigger informations, and executes a set of trades automatically depending on trigger values. The set of 
trades is selected from a *trade table* that you set beforehand, i.e., a computer doesn't decide what to trade, 
you do, the computer just executes them faster based on predefined rules. Website polling needs to be distributed, 
since there may be limits on how many GETs per second are allowed from one IP address.

# Architecture

Main points
- Onesec is backend code, written in Golang, and external API is gRPC based.
- User interface is not included, but can be written against a gRPC based API.
- Database of choice is MongoDB. Schema migration is handled by migrating the data, not complicating server code 
  to handle multiple schema versions. Migrations are Go code and they are integral part of each release.
- Distributed website polling is realised by running multiple Onesec instances in a data center or geographically 
  decentralized locations. Instances communicate using [RPC](https://pkg.go.dev/net/rpc). One of the instances acts 
  as a master executing trades, other instances are slaves polling for information. It's assumed that Onesec 
  instances communicate over a secure network, i.e., Onesec doesn't provide encryption. For multiple decentralized 
  locations, use 
  [VPN](https://www.digitalocean.com/community/tutorials/how-to-set-up-and-configure-an-openvpn-server-on-ubuntu-22-04)
  , for example.
- Distributed arbitrage algorithms are run using [RPC](https://pkg.go.dev/net/rpc).
- Method to serialise data to persistent storage depends on the amount of data.
    - If there's a lot of data, we use binary format and serialize a list of [protobuf](https://developers.google.com/protocol-buffers/docs/gotutorial) 
      message to disk. Protobufs work well with other languages, but if that's not needed, we use [gob](https://pkg.go.dev/encoding/gob) f
      or Go only access.
    - If the amount of data is small, we use common text based serialisations, like JSON or YAML.
- When Onesec is running in real time mode, it only concentrates on one thing, i.e.,
    - Acts as a master node for arbitrage
    - Acts as a slave node for arbitrage
    - Acts as a master node for news trading
    - Acts as a slave node for news trading
- Asset prices are streamed from Binance and saved to MongoDB for later analysis. When Onesec is run in analysis 
  mode, arbitrage possibilities can be scanned over longer asset loops and/or find profitable asset sets without 
  worrying about algorithm run time. Offline analysis can be used to select assets scanned real time, where 
  algorithm run time is heavily constrained. Analysis is done over a gRPC based API using 
  Python and [Jupyter](https://jupyter.org/), for example.

# Implementation order

Current code is a proof-of-concept that i) we can stream from Binance, ii) perform authenticated calls to Binance 
and iii) run Bellmann-Ford algorithm in real time.

Capabilities to implement
1. Initialise and shutdown
    - Startup
        - Supply a configuration file as input, and an optional override flag.
        - If the override flag is set, read configuration from the file and write to the database.
        - If the override flag is not set, read configuration from the database. If the database doesn't 
          contain configuration, fallback to the file, read it and write to the database.
    - Onesec is started in a run mode
    - Change run mode via gRPC.
    - Shutdown when commaned via gRPC.
2. Stream data
    - Connect to a Binance websocket and stream data.
    - Save data streams to the database.
    - Create query interface for the asset data in the database.
3. Run arbitrage algorithm
    - Run Bellmann-Ford in real time with a limited set of assets.
    - Run Bellmann-Ford in offline mode, reading data from the database. In offline mode we 
      can scan a large set of assets.

# Resources

Useful arbitrage links:

- https://anilpai.medium.com/currency-arbitrage-using-bellman-ford-algorithm-8938dcea56ea
- https://www.thealgorists.com/Algo/ShortestPaths/Arbitrage
- https://gist.github.com/ewancook/d39a06b2ee6e3f7c7c4d6cb66f2dcff7