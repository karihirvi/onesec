package shared

import (
	"github.com/spf13/cobra"
	"gitlab.com/karihirvi/onesec/internal/registry"
	"gitlab.com/karihirvi/onesec/internal/util"
)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run a trade loop.",
	Long: `Run a trade loop.

` + commonText + `

Usage
1) ./onesec run arbitrage
2) ./onesec run server --config configs/onesec.yaml,path/to/my_override_conf.yaml

`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		setupConfig()
		switch args[0] {
		case "arbitrage":
			util.LogPanicOnError(registry.RunArbitrage())
		case "server":
			srv := registry.NewDefaultServer()
			util.LogPanicOnError(srv.Start())
		default:
			exitUnknownCommand(args[0])
		}
	},
}

func init() {
	RootCmd.AddCommand(runCmd)
}
