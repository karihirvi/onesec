package shared

import (
	"github.com/spf13/cobra"
	"gitlab.com/karihirvi/onesec/internal/util"
)

var configFile string

const configFileFlag = "config"

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "onesec",
	Short: "Onesec",
	Long:  `Onesec is a live trading engine for exploiting arbitrage possibilities in crypto exchanges.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the RootCmd.
func Execute() {
	cobra.CheckErr(RootCmd.Execute())
}

func init() {
	RootCmd.PersistentFlags().StringVar(&configFile, configFileFlag, "", "Comma separated paths to config files")
	util.LogPanicOnError(RootCmd.MarkPersistentFlagRequired(configFileFlag))
}
