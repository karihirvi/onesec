package shared

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/karihirvi/onesec/internal/binance"
	"gitlab.com/karihirvi/onesec/internal/config"
	"gitlab.com/karihirvi/onesec/internal/global"
	"gitlab.com/karihirvi/onesec/internal/util"
	"os"
	"strings"
)

var credentialsVar, outVar string

const outFlag = "out"

const commonText = "All commands require --config=<comma separated list of config files> flag which \n" +
	"defines a list of config files read."

func setupConfig() *config.Config {
	configFiles := strings.Split(configFile, ",")
	conf, err3 := config.ReadConfig(configFiles)
	util.LogPanicOnError(err3)
	global.Init(conf)
	return conf
}

func exitUnknownCommand(cmd string) {
	util.LogPanicOnError(fmt.Errorf("unknown command %v", cmd))
}

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Get information from an exchange.",
	Long: `Get information from an exchange.
` + commonText + `

Usage
1) ./onesec get {exchange_info|account_info} [--out=PATH] --config=<PATH1, PATH2, ...>
   - 'exchange_info' gets exchange info.
   - 'account_info' gets account info, requires credentials.
   - 'credentials' option sets path to credentials file, required for some commands.
   - 'out' option saves output to a file instead of printing to console.

Examples
1) ./onesec get exchange_info --config=onesec.yaml
   - Gets exchange info and prints it to the console.
2) ./onesec get exchange_info -out info.json --config=onesec.yaml
	- Gets exchange info and saves it to info.json file in the current directory
2) ./onesec get account_info --config=onesec.yaml,override.yaml
   - Gets account info and prints it to the console. Use --out flag to save it to a file.
`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// configFiles := strings.Split(configFile, ",")
		// conf, err3 := config.ReadConfig(configFiles)
		// util.LogPanicOnError(err3)
		// util.LogPanicOnError(global.Init(conf))
		conf := setupConfig()
		var err error
		var body []byte
		switch args[0] {
		case "exchange_info":
			body, err = binance.GetExchangeInfo()
		case "account_info":
			cred, err2 := config.ReadCredentials(conf.Credentials)
			util.LogPanicOnError(err2)
			body, err = binance.GetAccountInfo(*cred)
		default:
			exitUnknownCommand(args[0])
		}
		util.LogPanicOnError(err)
		var prettyJson bytes.Buffer
		util.LogPanicOnError(json.Indent(&prettyJson, body, "", "  "))
		if cmd.Flags().Changed(outFlag) {
			util.LogPanicOnError(os.WriteFile(outVar, prettyJson.Bytes(), 0))
		} else {
			print(prettyJson.String())
		}
	},
}

func init() {
	RootCmd.AddCommand(getCmd)
	getCmd.Flags().StringVar(&outVar, outFlag, "", "Path to a file where output is saved.")
}
