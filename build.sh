#!/usr/bin/env sh

# Build script

# Save the initial directory.
dd=$(pwd)
# Generates protos
cd grpc || exit
. "$(pwd)"/env/grpc/bin/activate
sh ./generate.sh
deactivate
cd ..

# Build onesec
cd cmd/onesec || exit
go build -o ../../onesec .
# Go back to the initial directory.
cd "${dd}" || exit

# Build onesec_dev
cd cmd/onesec_dev || exit
go build -tags=test -o ../../onesec_dev .
# Go back to the initial directory.
cd "${dd}" || exit