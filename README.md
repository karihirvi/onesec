# Onesec

Onesec project is about enabling continuity trading for everyone. If everyone had the perfect market information 
and access to the market, asset prices would be in equilibrium all the time. Of course, this is not the 
case in real life, and we have discrepancies in asset pricing, i.e., pricing errors. Onesec seeks to exploit 
those fleeting moments of price discrepancies to make profit. For more in-depth explanation, 
please refer to [this document](docs/main.md).

Currently, the focus is on Binance crypto exchange and arbitrage possibilities between different exchange pairs.

Onesec provides a [gRPC](https://grpc.io/) based API for external control. User interface (GUI) is not 
included, but you use [protos](grpc) to generate a client for your programming language, e.g., python.

# Prerequisites

Onesec uses MongoDB as a database. If you want to run with a default configuration, MongoDB needs to be installed 
locally with default settings.

**Mac**
- To install MongoDB and tools.
    - `brew install mongodb-community mongodb-database-tools mongosh mongodb-compass`
- You can use `MongoDB Compass` GUI to access the database.

# Build

To build
- `./build.sh`

There are few options to run tests. 
1. Run unit tests, no special setup needed.
   - `./test.sh`
2. Run unit and integration tests. The latter requires a local MongoDB server with the default installation.
   - `./test.sh integration`
3. To run tests with coverage, add `coverage` to the command line parameters.
   - `./test.sh coverage` runs units test with coverage.
   - `./test.sh integration coverage` runs unit and integration tests with coverage.
   - After coverage tests pass, you can view a HTML coverage report by `go tool cover -html coverage.txt`

Inspect the scripts to see how to build and run tests manually.

# Run

To run with the default configuration only.
- `./onesec run server --config=configs/onesec.yaml`

# Development

Project layout (folder structure) follows [this standard.](https://github.com/golang-standards/project-layout)

**Architecture**
- Software complexity is managed by dependency injection, i.e., creating "standalone" packages that take 
  their dependencies in constructor(s).
- Configuration is defined in YAML files. A [default configuration](configs/onesec.yaml) is provided which
  can be overridden by any number of custom YAML files. A list of configuration files must be provided
  at command line when `onesec` is started. A configuration file may contain references to environment variables 
  in the form of `${MY_ENV_VARIABLE}`, and they will be expanded. 
- [global](internal/global) package contains globally available types and services, e.g., a logger and the configuration. 
  Packets, however, usually don't directly use the global services because we want to maximize independence with 
  dependency injection. For convenience, there's a [registry](internal/registry) package that provides 
  default constructors
  for all services. The default constructors initialises services using values from the global configuration.
  The [registry](internal/registry) package is also a place to check which packages are actually services, 
  since the "serviceness" is not reflected in package names.
- External API is [gRPC](https://grpc.io/) based. API definitions can be found in `protos` directory. To generate
  code based on the protos, run [generate.sh](grpc/generate.sh) in [protos](grpc) directory, 
  and a `protos/pb` directory appears. See also [build.sh](build.sh).

**Conventions**
- Packages contain general description what they do in `<packageName>.go` files. For example, a `server` package
  has a `server.go` file which contains an overview of the `server` package.
- Some parameters in the [default configuration](configs/onesec.yaml) include units. 
  For example, `state-collection-size-mb` and `exit-sleep-ms` have `-mb` and `-ms` suffixes. These refer to
  megabytes and milliseconds, respectively. However, fields in the [corresponding Go struct](internal/config/config.go)
  *don't* have suffixes. Struct fields represent data in a developer friendly way, i.e., `int64` bytes and 
  `time.Duration` nanoseconds. The rationale here is, that configuration files are geared towards human editing 
  whereas struct are geared towards developers (with syntax checking by the compiler). Besides the units, we 
  generally we want to maintain close resemblance between configuration and struct field naming. 
- gRPC API design aims to follow [this guide](https://cloud.google.com/apis/design)

**Testing**
- Run tests with the command below. Tests use shared code from `testutil` package, but we don't want that code to 
  be included in a production build. Thus, the`testing` tag.
    - `go test -race -tags=test ./...` 
